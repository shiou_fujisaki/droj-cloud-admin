import vue from '@vitejs/plugin-vue';
import { resolve } from 'path';
import { defineConfig } from "vite";
const path = require('path');// npm i --save-dev @types/node
const localEnabled = process.env.USE_MOCK || true;
const prodEnabled = process.env.USE_CHUNK_MOCK || false;
export default defineConfig({
    plugins: [vue()],
    build: {
        target: 'modules',
        outDir: 'dist',
        assetsDir: 'assets',
        minify: 'terser',
    },
    // 配置别名
    resolve: {
        extensions: ['.js','.vue','.json'],
        alias: { '@': path.resolve(__dirname, './src')}
    },
    base: './',
    optimizeDeps: {
        include: ['schart.js']
    },
    hmr:true,
})
chainWeblack: config => {
    // 修复HMR
    config.resolve.symlinks(true);
}
devServer: {
    host: '0.0.0.0';
    hot: true;
    https: false;
    allowedHosts: [
      'myhost.mydomain.com'
    ]
}