const sidebar = [
	{ icon: "el-icon-lx-home", title: "系统首页", index: "/dashboard", },
	// ----------------------------------------------------------------------------------------------- 站点管理 START
	{ icon: "el-icon-lx-global", title: "站点服务", index: "/webC",
		subs: [
			{ icon: "el-icon-lx-global", title: "站点配置", index: "/webSiteInfo", },
			{ icon: "el-icon-lx-global", title: "广告位管理", index: "/adverts",
				subs: [
					{ title: "广告位列表", index: "/adverts",  },
					{ title: "首屏图", index: "/openScreen",  },
					{ title: "轮播图", index: "/rotationMap",  }
				]
			}
		]
	},
	// ----------------------------------------------------------------------------------------------- 站点管理 END
	// ----------------------------------------------------------------------------------------------- 新闻资讯 START
	{ icon: "el-icon-lx-news", title: "新闻中心", index: "/newsC",
		subs: [
			{ icon: "el-icon-lx-news", title: "新闻资讯", index: "/news",
				subs: [
					{ title: "资讯列表", index: "/news",  },
					{ title: "发布稿件", index: "/addNews?newsId=0&load=0",  }
				]
			},
			{ icon: "el-icon-present", title: "资讯栏目", index: "/newsCatalog",
				subs: [
					{ title: "资讯栏目列表", index: "/newsCatalog",  },
					{ title: "资讯标签列表", index: "/newsTag",  }
				]
			}
		]
	},
	// ----------------------------------------------------------------------------------------------- 新闻资讯 END
	{ icon: "el-icon-lx-record", title: "媒体中心", index: "/mediaC",
		subs: [
			{ title: "数据仓库", index: "/mediaC",  },
			{ title: "数据中心", index: "/dataC",  }
		]
	},
	// ----------------------------------------------------------------------------------------------- 数字商城 START
	{ icon: "el-icon-lx-shop", title: "数字商城", index: "/shopC",
		subs: [
			// {
			//     icon: "el-icon-box",
			//     title: "商户大厅",
			//     index: "/custom",
			//     subs: [
			//         {
			//             index: "/customer",
			//             title: "平台商户",
			//         },
			//         {
			//             index: "/customerAdd",
			//             title: "商户注册",
			//         },
			//         // {
			//         //     index: "/product",
			//         //     title: "平台商品",
			//         // },
			//         // {
			//         //     index: "/productOcr",
			//         //     title: "商品OCR识别",
			//         // },
			//     ],
			// },
			{ icon: "el-icon-lx-shop", title: "品牌管理", index: "/boards",
				subs: [
					{ title: "品牌列表", index: "/board",  },
				]
			},
			{ icon: "el-icon-lx-shop", title: "商品类目管理", index: "/productProject",
				subs: [
					{ index: "/classify", title: "商品类目管理",  },
				]
			},
			{ icon: "el-icon-lx-shop", title: "商品管理", index: "/products",
				subs: [
					{ index: "/products", title: "商品列表",  },
					{ index: "/productGuide", title: "新建商品",  },
					{ index: "/productRecovery", title: "产品回收站",  },
				]
			},
		]
	},
	// ----------------------------------------------------------------------------------------------- 数字商城 END
	{ icon: "el-icon-lx-shop", title: "营销活动", index: "/marketing", 
	subs: [
		{ title: "积分活动", index: "/activity"},
		{ title: "积分活动规则", index: "/activityRule",  },
		{ title: "优惠券管理", index: "/coupons",  },
		{ title: "会员折扣", index: "/huiyuanDis",  },
		{ title: "拼团折扣", index: "/pintuanDis",  },
		{ title: "经销商加盟折扣", index: "/jingxiaoshangDis",  }
	]
	},
	{ icon: "el-icon-lx-shop", title: "订单管理", index: "/orders", 
		subs: [
			{ title: "订单列表", index: "/orders",  },
			{ title: "新建商品订单", index: "/takeGoodsOrder",  },
		]
	},
	// ----------------------------------------------------------------------------------------------- 会员管理 START
	{ icon: "el-icon-postcard", title: "会员中心", index: "/memberC",
		subs: [
			{ icon: "el-icon-postcard", title: "会员管理", index: "/memberInfo", 
				subs: [
					{ title: "会员列表", index: "/member",  },
					{ title: "会员设备", index: "/memberDevice",  },
					{ title: "会员积分", index: "/memberIntel",  },
					{ title: "会员黑名单", index: "/memberForbiden",  }
				],
			},
			{ icon: "el-icon-postcard",	title: "会员等级", index: "/memberGrade",  },
			{ icon: "el-icon-postcard", title: "会员话题留言", index: "/memberSubject",  }
		]
	},
	// ----------------------------------------------------------------------------------------------- 会员管理 END
	// ----------------------------------------------------------------------------------------------- U剪数据 START
	{ icon: "el-icon-lx-goods", title: "U剪服务中心", index: "/hairmanager",
		subs: [
			{ icon: "el-icon-lx-goods", title: "工作室", index: "/workroom",  },
			{ icon: "el-icon-lx-goods", title: "发型师", index: "/barber",  },
			{ icon: "el-icon-lx-goods", title: "剪发服务", index: "/serviceProject",
				subs: [
					{ title: "服务项目列表", index: "/serviceProject",  },
					{ title: "新建服务项目", index: "/addServiceProject",  },
					{ title: "服务项目类目管理", index: "/configServiceParentProject",  }
				],
			}
		]
	},
	// -----------------------------------------------------------------------------------------------u剪数据 END
	// ----------------------------------------------------------------------------------------------- SysAdmin管理界面 START
	{ icon: "el-icon-lx-settings", title: "控制台", index: "/uc",
		subs: [
			{ icon: "el-icon-lx-settings", title: "系统设置", index: "/settings", 
				subs: [
					{ index: "/dictionary", title: "系统字典索引"},
					{ index: "/sysinfo", title: "平台参数" },
					{ index: "/dform", title: "定制表单"}
				]
			}
		]
	},
	// ----------------------------------------------------------------------------------------------- SysAdmin管理界面 END

];

const sidebar2 = [
	{
		icon: "el-icon-lx-home",
		title: "系统首页",
		index: "/dashboard",
	},
	// ----------------------------------------------------------------------------------------------- SysAdmin管理界面 START
	{
		icon: "el-icon-lx-settings",
		title: "控制台",
		index: "/uc",
		subs: [
			{ icon: "el-icon-profile", index: "/tenant", title: "平台租户"},
			{ index: "/mpermission", title: "管理员操作权限"},
			{ index: "/mresouse", title: "管理员资源菜单"},
			{ icon: "el-icon-lx-settings", index: "/mrole", title: "管理员角色", subs: [
					{ index: "/mrole", title: "系统角色"},
					{ index: "/roleBundMembers", title: "角色绑定用户"},
					{ index: "/roleBundResouces", title: "角色绑定资源"},
					{ index: "/roleBundPermissions", title: "角色绑定权限"},
				],
			},
			{ icon: "el-icon-lx-settings", title: "系统设置", index: "/settings", subs: [
					{ index: "/dictionary", title: "系统字典索引"},
					{ index: "/sysinfo", title: "平台参数"},
				],
			},
		],
	},
	// ----------------------------------------------------------------------------------------------- SysAdmin管理界面 END
	// ----------------------------------------------------------------------------------------------- 开发样板界面 START
	{
		icon: "el-icon-lx-settings",
		title: "开发界面",
		index: "/devloping",
		subs: [
			{ icon: "el-icon-lx-home", index: "/drawer", title: "抽屉页"},
			{ icon: "el-icon-lx-cascades", index: "/table", title: "基础表格"},
			{ icon: "el-icon-lx-copy", index: "/tabs", title: "tab选项卡"},
			{ icon: "el-icon-lx-calendar", index: "3", title: "表单相关", subs: [
					{ index: "/form", title: "基本表单"},
					{ index: "/upload", title: "文件上传"},
					{ index: "4", title: "三级菜单", subs: [ { index: "/editor", title: "富文本编辑器"}]}
				]
			},
			{ icon: "el-icon-lx-emoji", index: "/icon", title: "自定义图标", },
			{ icon: "el-icon-pie-chart", index: "/charts", title: "schart图表", },
			{ icon: "el-icon-lx-global", index: "/i18n", title: "国际化功能", },
			{ icon: "el-icon-lx-warn", index: "7", title: "错误处理", subs: [
					{ index: "/permission", title: "权限测试", },
					{ index: "/404", title: "404页面", },
				]
			},
			{ icon: "el-icon-lx-redpacket_fill", index: "/donate", title: "支持作者",subs:[] }
		]
	}
	// ----------------------------------------------------------------------------------------------- 开发样板界面 END
];
export {
	sidebar,sidebar2
};