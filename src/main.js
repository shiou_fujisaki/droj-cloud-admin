import {createApp} from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import installElementPlus from './plugins/element';
import ElementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';
import * as echarts from 'echarts';
import './assets/css/icon.css';
const app = createApp(App)
installElementPlus(app)
app
    .use(store)
    .use(router)
    .use(ElementPlus)
    .mount('#app')
app.echarts=echarts