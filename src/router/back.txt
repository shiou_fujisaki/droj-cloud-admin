/**
 * renter_route 租户管理路由
 */
 export const renter_Routes = [
    {
        path: "/",
        redirect: "/dashboard"
    },
    {
        path: "/",
        name: "Home",
        component: Home,
        children: [
            {
                path: "/dashboard",
                name: "dashboard",
                meta: {
                    title: "系统首页"
                },
                component: () => import ( /* webpackChunkName: "dashboard" */ "../views/renterDashboard.vue")
            }, 
            // -----------------------------------------------------------------------------------------------SysAdmin管理界面
            {
                path: "/tenant",
                name: "tenant",
                meta: {
                    title: "平台租户"
                },
                component: () => import ( /* webpackChunkName: "tenant" */ "../views/renter/tenant/index.vue")
            },
            {
                path: "/mrole",
                name: "mrole",
                meta: {
                    title: "管理员角色"
                },
                component: () => import ( /* webpackChunkName: "mrole" */ "../views/renter/role/index.vue")
            },
            {
                path: "/roleBundMembers",
                name: "roleBundMembers",
                meta: {
                    title: "管理员角色"
                },
                component: () => import ( /* webpackChunkName: "roleBundMembers" */ "../views/renter/role/roleBundMembers.vue")
            },
            {
                path: "/roleBundResouces",
                name: "roleBundResouces",
                meta: {
                    title: "管理员操作资源"
                },
                component: () => import ( /* webpackChunkName: "roleBundResouces" */ "../views/renter/role/roleBundResouces.vue")
            },
            {
                path: "/roleBundPermissions",
                name: "roleBundPermissions",
                meta: {
                    title: "管理员操作资源"
                },
                component: () => import ( /* webpackChunkName: "roleBundPermissions" */ "../views/renter/role/roleBundPermissions.vue")
            },
			{
                path: "/mpermission",
                name: "mpermission",
                meta: {
                    title: "平台权限"
                },
                component: () => import ( /* webpackChunkName: "mpermission" */ "../views/renter/permission/index.vue")
            },
            {
                path: "/mresouse",
                name: "mresouse",
                meta: {
                    title: "平台资源菜单"
                },
                component: () => import ( /* webpackChunkName: "mresouse" */ "../views/renter/resouses/index.vue")
            },
            {
                path: "/mediaC",
                name: "mediaC",
                meta: {
                    title: "多媒体数据仓库"
                },
                component: () => import ( /* webpackChunkName: "mediaC" */ "../views/sysadmin/mediac.vue")
            },
            {
                path: "/dataC",
                name: "dataC",
                meta: {
                    title: "数据仓库"
                },
                component: () => import ( /* webpackChunkName: "dataC" */ "../views/sysadmin/datac.vue")
            },
            {
                path: "/dictionaryIndex",
                name: "dictionaryIndex",
                meta: {
                    title: "系统字典"
                },
                component: () => import ( /* webpackChunkName: "dictionaryIndex" */ "@/views/sysadmin/dictionaryIndex.vue")
            },
            {
                path: "/dictionary",
                name: "dictionary",
                meta: {
                    title: "有效系统字典"
                },
                component: () => import ( /* webpackChunkName: "dictionary" */ "@/views/sysadmin/dictionary.vue")
            },
            {
                path: "/delDictionary",
                name: "delDictionary",
                meta: {
                    title: "无效系统字典"
                },
                component: () => import ( /* webpackChunkName: "delDictionary" */ "@/views/sysadmin/delDictionary.vue")
            },
            // ----------------------------------------------------------------------------------------------- SysAdmin管理界面
            // ----------------------------------------------------------------------------------------------- 开发样板界面
            {
                path: "/drawer",
                name: "drawer",
                meta: {
                    title: "抽屉页"
                },
                component: () => import ( /* webpackChunkName: "dashboard" */ "../views/devloping/drawer.vue")
            }
            , {
                path: "/table",
                name: "basetable",
                meta: {
                    title: "表格"
                },
                component: () => import ( /* webpackChunkName: "table" */ "../views/devloping/BaseTable.vue")
            }, {
                path: "/charts",
                name: "basecharts",
                meta: {
                    title: "图表"
                },
                component: () => import ( /* webpackChunkName: "charts" */ "../views/devloping/BaseCharts.vue")
            }, {
                path: "/form",
                name: "baseform",
                meta: {
                    title: "表单"
                },
                component: () => import ( /* webpackChunkName: "form" */ "../views/devloping/BaseForm.vue")
            }, {
                path: "/tabs",
                name: "tabs",
                meta: {
                    title: "tab标签"
                },
                component: () => import ( /* webpackChunkName: "tabs" */ "../views/devloping/Tabs.vue")
            }, {
                path: "/donate",
                name: "donate",
                meta: {
                    title: "鼓励作者"
                },
                component: () => import ( /* webpackChunkName: "donate" */ "../views/devloping/Donate.vue")
            }, {
                path: "/permission",
                name: "permission",
                meta: {
                    title: "权限管理",
                    permission: true
                },
                component: () => import ( /* webpackChunkName: "permission" */ "../views/devloping/Permission.vue")
            }, {
                path: "/i18n",
                name: "i18n",
                meta: {
                    title: "国际化语言"
                },
                component: () => import ( /* webpackChunkName: "i18n" */ "../views/devloping/I18n.vue")
            }, {
                path: "/upload",
                name: "upload",
                meta: {
                    title: "上传插件"
                },
                component: () => import ( /* webpackChunkName: "upload" */ "../views/devloping/Upload.vue")
            }, {
                path: "/icon",
                name: "icon",
                meta: {
                    title: "自定义图标"
                },
                component: () => import ( /* webpackChunkName: "icon" */ "../views/devloping/Icon.vue")
            }, {
                path: "/404",
                name: "404",
                meta: {
                    title: "找不到页面"
                },
                component: () => import (/* webpackChunkName: "404" */ "../views/devloping/404.vue")
            }, {
                path: "/403",
                name: "403",
                meta: {
                    title: "没有权限"
                },
                component: () => import (/* webpackChunkName: "403" */ "../views/devloping/403.vue")
            }, {
                path: "/user",
                name: "user",
                meta: {
                    title: "个人中心"
                },
                component: () => import (/* webpackChunkName: "user" */ "../views/devloping/User.vue")
            }, {
                path: "/editor",
                name: "editor",
                meta: {
                    title: "富文本编辑器"
                },
                component: () => import (/* webpackChunkName: "editor" */ "../views/devloping/Editor.vue")
            }
            // ----------------------------------------------------------------------------------------------- 开发样板界面
        ]
    }, {
        path: "/login",
        name: "Login",
        meta: {
            title: "登录"
        },
        component: () => import ( /* webpackChunkName: "login" */ "../views/Login.vue")
    }
];
/**
 * renter_route 用户端操作路由
 */
 export const um_routes = [
    {
        path: "/selectSites",
        name: "selectSites",
        meta: {
            title: "选择业务站点"
        },
        component: () => import ( /* webpackChunkName: "selectSites" */ "../views/SelectSites.vue")
    }, 
    {
        path: "/",
        redirect: "/dashboard"
    },
    {
        path: "/",
        name: "Home",
        component: Home,
        children: [
            {
                path: "/dashboard",
                name: "dashboard",
                meta: {
                    title: "系统首页"
                },
                component: () => import ( /* webpackChunkName: "dashboard" */ "../views/memberDashboard.vue")
            }, 
            // -----------------------------------------------------------------------------------------------平台商户
            {
                path: "/customer",
                name: "customer",
                meta: {
                    title: "平台商户"
                },
                component: () => import ( /* webpackChunkName: "customer" */ "../views/shopper/index.vue")
            },
            {
                path: "/customerAdd",
                name: "customerAdd",
                meta: {
                    title: "商户注册"
                },
                component: () => import ( /* webpackChunkName: "customerAdd" */ "../views/shopper/customer.vue")
            },
            {
                path: "/product",
                name: "product",
                meta: {
                    title: "平台商品"
                },
                component: () => import ( /* webpackChunkName: "product" */ "../views/shopper/cproduct.vue")
            },
            {
                path: "/productOcr",
                name: "productOcr",
                meta: {
                    title: "商品OCR识别"
                },
                component: () => import ( /* webpackChunkName: "productOcr" */ "../views/ocr/ocrProduct.vue")
            },
            // -----------------------------------------------------------------------------------------------平台商户
            // -----------------------------------------------------------------------------------------------新闻资讯
            {
                path: "/newsCatalog",
                name: "newsCatalog",
                meta: {
                    title: "资讯栏目列表"
                },
                component: () => import ( /* webpackChunkName: "newsCatalog" */ "../views/hairmanager/newsCatalog/index.vue")
            },
            {
                path: "/newsTag",
                name: "newsTag",
                meta: {
                    title: "资讯标签列表"
                },
                component: () => import ( /* webpackChunkName: "newsTag" */ "../views/hairmanager/newsTag/index.vue")
            },
            {
                path: "/news",
                name: "news",
                meta: {
                    title: "资讯列表"
                },
                component: () => import ( /* webpackChunkName: "news" */ "../views/hairmanager/news/index.vue")
            },
            {
                path: "/addNews",
                name: "addNews",
                meta: {
                    title: "发布稿件"
                },
                component: () => import ( /* webpackChunkName: "addNews" */ "../views/hairmanager/news/components/newsForm.vue")
            },
            {
                path: "/adverts",
                name: "adverts",
                meta: {
                    title: "广告位列表"
                },
                component: () => import ( /* webpackChunkName: "adverts" */ "../views/hairmanager/advert/index.vue")
            },
            {
                path: "/openScreen",
                name: "openScreen",
                meta: {
                    title: "开屏图配置"
                },
                component: () => import ( /* webpackChunkName: "openScreen" */ "../views/hairmanager/advert/OpenScreenIndex.vue")
            },
            {
                path: "/rotationMap",
                name: "rotationMap",
                meta: {
                    title: "轮播图配置"
                },
                component: () => import ( /* webpackChunkName: "rotationMap" */ "../views/hairmanager/advert/RotationMapIndex.vue")
            },
            {
                path: "/webSiteInfo",
                name: "webSiteInfo",
                props: true,
                meta: {
                    title: "站点配置"
                },
                component: () => import ( /* webpackChunkName: "webSiteInfo" */ "../views/hairmanager/webSiteInfo.vue")
            },
            // -----------------------------------------------------------------------------------------------新闻资讯
            // -----------------------------------------------------------------------------------------------会员管理
            // 用户中心
            {
                path: "/member",
                name: "member",
                meta: {
                    title: "用户中心"
                },
                component: () => import ( /* webpackChunkName: "member" */ "../views/hairmanager/member/index.vue")
            }, 
            {
                path: "/memberGrade",
                name: "memberGrade",
                props: true,
                meta: {
                    title: "会员等级"
                },
                component: () => import ( /* webpackChunkName: "memberGrade" */ "../views/hairmanager/memberGrade/index.vue")
            },
            {
                path: "/memberDevice",
                name: "memberDevice",
                props: true,
                meta: {
                    title: "会员设备"
                },
                component: () => import ( /* webpackChunkName: "memberDevice" */ "../views/hairmanager/memberDevice/index.vue")
            },
            {
                path: "/memberIntel",
                name: "memberIntel",
                props: true,
                meta: {
                    title: "会员积分"
                },
                component: () => import ( /* webpackChunkName: "memberIntel" */ "../views/hairmanager/memberIntel/index.vue")
            },
            {
                path: "/memberForbiden",
                name: "memberForbiden",
                meta: {
                    title: "禁用会员列表"
                },
                component: () => import ( /* webpackChunkName: "memberForbiden" */ "../views/hairmanager/member/fixMembers.vue")
            }, 
            {
                path: "/memberSubject",
                name: "memberSubject",
                meta: {
                    title: "禁用会员列表"
                },
                component: () => import ( /* webpackChunkName: "memberSubject" */ "../views/hairmanager/memberSubject/index.vue")
            }, 
            // -----------------------------------------------------------------------------------------------会员管理
            // -----------------------------------------------------------------------------------------------数字商城
            {
                path: "/activity",
                name: "activity",
                meta: {
                    title: "积分活动"
                },
                component: () => import ( /* webpackChunkName: "activity" */ "../views/hairmanager/activity/index.vue")
            },
            {
                path: "/activityRule",
                name: "activityRule",
                meta: {
                    title: "积分活动规则"
                },
                component: () => import ( /* webpackChunkName: "activityRule" */ "../views/hairmanager/activityRule/index.vue")
            },
            {
                path: "/workroom",
                name: "workroom",
                meta: {
                    title: "工作室"
                },
                component: () => import ( /* webpackChunkName: "workroom" */ "../views/hairmanager/workroom/index.vue")
            },
            {
                path: "/workRoomConfig",
                name: "workRoomConfig",
                props: true,
                meta: {
                    title: "工作配置室"
                },
                component: () => import ( /* webpackChunkName: "workRoomConfig" */ "../views/hairmanager/workroom/workRoomConfig.vue")
            },
            {
                path: "/serviceProject",
                name: "serviceProject",
                props: true,
                meta: {
                    title: "服务项目"
                },
                component: () => import ( /* webpackChunkName: "serviceProject" */ "../views/hairmanager/serviceProject/index.vue")
            },
            {
                path: "/addServiceProject",
                name: "addServiceProject",
                props: true,
                meta: {
                    title: "新建服务项目"
                },
                component: () => import ( /* webpackChunkName: "addServiceProject" */ "../views/hairmanager/serviceProject/add.vue")
            },
            {
                path: "/configServiceParentProject",
                name: "configServiceParentProject",
                props: true,
                meta: {
                    title: "服务项目类目"
                },
                component: () => import ( /* webpackChunkName: "configServiceParentProject" */ "../views/hairmanager/serviceProject/ServiceProjectParentConfig.vue")
            },
            {
                path: "/barber",
                name: "barber",
                props: true,
                meta: {
                    title: "发型师"
                },
                component: () => import ( /* webpackChunkName: "barber" */ "../views/hairmanager/barber/index.vue")
            },
            {
                path: "/board",
                name: "board",
                props: true,
                meta: {
                    title: "品牌管理"
                },
                component: () => import ( /* webpackChunkName: "board" */ "../views/hairmanager/board/index.vue")
            },
            {
                path: "/boardGuide",
                name: "boardGuide",
                props: true,
                meta: {
                    title: "品牌管理"
                },
                component: () => import ( /* webpackChunkName: "boardGuide" */ "../views/hairmanager/board/components/boardForm.vue")
            },
            {
                path: "/products",
                name: "products",
                props: true,
                meta: {
                    title: "商品管理"
                },
                component: () => import ( /* webpackChunkName: "products" */ "../views/hairmanager/product/index.vue")
            },
            {
                path: "/productDetail",
                name: "productDetail",
                props: true,
                meta: {
                    title: "商品项目详情"
                },
                component: () => import ( /* webpackChunkName: "productDetail" */ "../views/hairmanager/product/productDetail.vue")
            },
            {
                path: "/productSpec",
                name: "productSpec",
                props: true,
                meta: {
                    title: "商品规则属性配置"
                },
                component: () => import ( /* webpackChunkName: "productSpec" */ "../views/hairmanager/product/components/productSpec.vue")
            },
            {
                path: "/productGuide",
                name: "productGuide",
                props: true,
                meta: {
                    title: "新建商品"
                },
                component: () => import ( /* webpackChunkName: "productGuide" */ "../views/hairmanager/product/productGuide.vue")
            },
            {
                path: "/productRecovery",
                name: "productRecovery",
                props: true,
                meta: {
                    title: "产品回收站"
                },
                component: () => import ( /* webpackChunkName: "productRecovery" */ "../views/hairmanager/product/productRecovery.vue")
            },
            {
                path: "/classify",
                name: "classify",
                props: true,
                meta: {
                    title: "商品类目管理"
                },
                component: () => import ( /* webpackChunkName: "classify" */ "../views/hairmanager/classify/index.vue")
            },
            {
                path: "/productAttribute",
                name: "productAttribute",
                props: true,
                meta: {
                    title: "商品属性管理"
                },
                component: () => import ( /* webpackChunkName: "productAttribute" */ "../views/hairmanager/productAttribute/index.vue")
            },
            {
                path: "/orders",
                name: "orders",
                props: true,
                meta: {
                    title: "订单管理"
                },
                component: () => import ( /* webpackChunkName: "orders" */ "../views/hairmanager/order/index.vue")
            },
            {
                path: "/takeGoodsOrder",
                name: "takeGoodsOrder",
                props: true,
                meta: {
                    title: "新建商品订单"
                },
                component: () => import ( /* webpackChunkName: "takeGoodsOrder" */ "@/views/hairmanager/order/orderForm.vue")
            },
            {
                path: "/coupons",
                name: "coupons",
                props: true,
                meta: {
                    title: "优惠券管理"
                },
                component: () => import ( /* webpackChunkName: "coupons" */ "@/views/hairmanager/coupon/index.vue")
            },
            // -----------------------------------------------------------------------------------------------数字商城
            // ----------------------------------------------------------------------------------------------- SysAdmin管理界面
            {
                path: "/dictionaryIndex",
                name: "dictionaryIndex",
                meta: {
                    title: "系统字典"
                },
                component: () => import ( /* webpackChunkName: "dictionaryIndex" */ "@/views/sysadmin/dictionaryIndex.vue")
            },
            {
                path: "/dictionary",
                name: "dictionary",
                meta: {
                    title: "有效系统字典"
                },
                component: () => import ( /* webpackChunkName: "dictionary" */ "@/views/sysadmin/dictionary.vue")
            },
            {
                path: "/delDictionary",
                name: "delDictionary",
                meta: {
                    title: "无效系统字典"
                },
                component: () => import ( /* webpackChunkName: "delDictionary" */ "@/views/sysadmin/delDictionary.vue")
            }
            // ----------------------------------------------------------------------------------------------- SysAdmin管理界面
        ]
    }, {
        path: "/login",
        name: "Login",
        meta: {
            title: "登录"
        },
        component: () => import ( /* webpackChunkName: "login" */ "../views/Login.vue")
    }
];

const routes = [{
    path: "/login",
    name: "Login",
    meta: {
        title: '登录'
    },
    component: () => import ( /* webpackChunkName: "login" */ "../views/Login.vue")
}];