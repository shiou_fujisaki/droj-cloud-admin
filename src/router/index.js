import {createRouter, createWebHashHistory} from "vue-router";
import {renter_Routes, um_routes} from "@/components/datas/RouterData";
import store from "@/store";
const routes = [
    { path: "/login", name: "Login", meta: { title: '登录' }, component: () => import ( /* webpackChunkName: "login" */ "@/views/Login.vue") },
    { path: "/selectSites", name: "selectSites", meta: { title: "选择业务站点" }, component: () => import ( /* webpackChunkName: "selectSites" */ "@/views/SelectSites.vue") },
    { path: "/tabs", name: "tabs", meta: { title: "tab标签" }, component: () => import ( /* webpackChunkName: "tabs" */ "@/views/devloping/Tabs.vue") },
];
const _root = localStorage.getItem('region') === "1" ? renter_Routes : localStorage.getItem('region') === "0" ? um_routes : routes;
export const router = createRouter({
    history: createWebHashHistory(),
    routes: _root
});

router.beforeEach((to, from, next) => {
    document.title = `${to.meta.title} | vue-manage-system`;
    let isLoadRouters = store.state.asyncRoutesMark;
    let token = localStorage.getItem("Authorization");
    const loginer = localStorage.getItem('ms_username');
    if(!token && to.path !== '/login') {
        next('/login');
    } else {
        // 用户已经登录
        if(token){
        // 如果是管理员权限则可进入，这里只是简单的模拟管理员权限而已
        //if (to.meta.permission) {
            next();
        } else {
            // 强制跳转登录信息
            store.commit('setMenuList',[]);
            store.commit('setAsyncRoutesMark',false);
            localStorage.setItem("Authorization","");
            sessionStorage.setItem("menuList", JSON.stringify([]));
            next()
        }
    }
});

export default router;