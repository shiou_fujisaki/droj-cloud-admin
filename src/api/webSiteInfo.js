import request from '../utils/request';
import AjaxRequest from '../utils/AjaxRequest';
//  --------------------------------------------------------------------------------------------------------------- //
// 站点信息列表分页查询
export const findSiteInfoListPage = query => {
    return AjaxRequest.request({
        url: '/siteInfo/findSiteInfoListPage',
        method: 'get',
		params: query,
    });
};
// 站点信息详情查询
export const findSiteInfoInfoById = query => {
    return AjaxRequest.request({
        url: '/siteInfo/findSiteInfoInfoById/'+query,
        method: 'get',
		params: query,
    });
};
// 站点信息详情查询
export const findSiteInfoInfoByAuthCode = query => {
    return AjaxRequest.request({
        url: '/siteInfo/findSiteInfoInfoByAuthCode/'+query,
        method: 'get',
		params: query,
    });
};
// 新增站点信息
export const addSiteInfo = data => {
    return AjaxRequest.request({
        url: '/siteInfo/saveSiteInfo',
        method: 'post',
		data: data,
    });
};
// 编辑站点信息
export const saveSiteInfo = data => {
    return AjaxRequest.request({
        url: '/siteInfo/saveSiteInfo',
        method: 'put',
		data: data,
    });
};
// 删除站点信息
export const deleteSiteInfo = data => {
    return AjaxRequest.request({
        url: '/siteInfo/deleteSiteInfo/'+data,
        method: 'delete',
    });
};