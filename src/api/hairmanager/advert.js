import request from '../../utils/request';
import AjaxRequest from '../../utils/AjaxRequest';
/**
* Tools 请求工具
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// Advert 广告位 --------------------------------------------------------------------Start
const advert_url = "/admin/advert";
//  --------------------------------------------------------------------------------------------------------------- //
// 广告位列表分页查询
export const findAdvertListPage = query => {
    return AjaxRequest.request({
        url: advert_url+'/findAdvertListPage',
        method: 'get',
		params: query,
    });
};
// 广告位详情查询
export const findAdvertInfoById = query => {
    return AjaxRequest.request({
        url: advert_url+'/findAdvertInfoById/'+query,
        method: 'get',
		params: query,
    });
};
// 新增广告位
export const addAdvert = data => {
    return AjaxRequest.request({
        url: advert_url+'/saveAdvert',
        method: 'post',
		data: data,
    });
};
// 编辑广告位
export const saveAdvert = data => {
    return AjaxRequest.request({
        url: advert_url+'/saveAdvert',
        method: 'put',
		data: data,
    });
};
// 删除广告位
export const deleteAdvert = data => {
    return AjaxRequest.request({
        url: advert_url+'/deleteAdvert/'+data,
        method: 'delete',
    });
};
// 批量删除广告位
export const batchDeleteAdvert = data => {
    return AjaxRequest.request({
        url: advert_url+'/batchDeleteAdvert',
        method: 'delete',
        data: data
    });
};
// 广告位状态批量更新
export const batchEditStatus = data => {
    return AjaxRequest.request({
        url: advert_url+'/batchEditStatus',
        method: 'put',
		data: data,
    });
};