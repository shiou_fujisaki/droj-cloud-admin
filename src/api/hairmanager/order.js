import request from '../../utils/request';
import AjaxRequest from '../../utils/AjaxRequest';
/**
 * Tools 请求工具
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// Order 订单 --------------------------------------------------------------------Start
const order_url = "/admin/order";
//  --------------------------------------------------------------------------------------------------------------- //
// 订单列表分页查询
export const findOrderListPage = query => {
    return AjaxRequest.request({
        url: '/order/findOrderListPage',
        method: 'get',
		params: query,
    });
};
// 订单详情查询
export const findOrderInfoById = query => {
    return AjaxRequest.request({
        url: '/order/info/'+query,
        method: 'get',
		params: query,
    });
};
// 新增订单
export const addGoodsOrder = data => {
    return AjaxRequest.request({
        url: '/order/saveGoodsOrder',
        method: 'post',
		data: data,
    });
};
// 编辑订单
export const saveGoodsOrder = data => {
    return AjaxRequest.request({
        url: '/order/saveGoodsOrder',
        method: 'put',
		data: data,
    });
};

// 批量删除订单
export const batchEditOrderStatus = data => {
    return AjaxRequest.request({
        url: '/order/batchEditOrderStatus',
        method: 'put',
        data: data
    });
};