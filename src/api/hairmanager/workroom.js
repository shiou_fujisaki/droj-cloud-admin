import request from '../../utils/request';
import AjaxRequest from '../../utils/AjaxRequest';
/**
 * Tools 请求工具
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// Service 服务项目 --------------------------------------------------------------------Start
const service_url = "/admin/workroom";
//  --------------------------------------------------------------------------------------------------------------- //
// 工作室列表查询
export const fetchWorkRoomListData = query => {
    return AjaxRequest.request({
        url: service_url+'/findWorkRoomListPage',
        method: 'get',
        params: query
    });
};
// 工作室详细查询
export const fetchWorkRoomDetailData = query => {
    return AjaxRequest.request({
        url: service_url+'/findWorkRoomInfoById/'+query,
        method: 'get'
    });
};
// 保存工作室数据
export const saveWorkRoom = data => {
    return AjaxRequest.request({
        url: service_url+'/saveWorkRoom',
        method: 'post',
        data: data
    });
};
// 删除工作室数据
export const deleteWorkRoomById = data => {
    return AjaxRequest.request({
        url: service_url+'/deleteWorkRoomById/'+data,
        method: 'post'
    });
};
//  --------------------------------------------------------------------------------------------------------------- //
// 获取工作室手艺人列表信息
export const findWorkRoomBarberListPage = query => {
    return AjaxRequest.request({
        url: service_url+'/findWorkRoomBarberList/'+query,
        method: 'get',
    });
};
// 获取工作室服务项目列表信息
export const findWorkRoomServiceProjectListPage = query => {
    return AjaxRequest.request({
        url: service_url+'/findWorkRoomServiceProjectList/'+query,
        method: 'get',
    });
};
// 获取工作室发型师服务项目价格列表信息
export const findWorkRoomBarberProjectPriceListPage = query => {
    return AjaxRequest.request({
        url: service_url+'/findWorkRoomBarberProjectPriceList/'+query,
        method: 'get',
    });
};
//  --------------------------------------------------------------------------------------------------------------- //
// 工作室添加理发师并给服务项目设置金额
export const configWorkRoomBarberProjectPriceInfo = data => {
    return AjaxRequest.request({
        url: service_url+'/configWorkRoomBarberProjectPriceInfo',
        method: 'post',
        data: data
    });
};
// 工作室配置服务项目
export const configWorkRoomProject = data => {
    return AjaxRequest.request({
        url: service_url+'/configWorkRoomProject',
        method: 'post',
        data: data
    });
};
// 工作室配置发型师
export const configWorkRoomBarber = data => {
    return AjaxRequest.request({
        url: service_url+'/configWorkRoomBarber',
        method: 'post',
        data: data
    });
};
//  --------------------------------------------------------------------------------------------------------------- //
// 工作室取消服务项目
export const cleanWorkRoomProject = data => {
    return AjaxRequest.request({
        url: service_url+'/cleanWorkRoomProject',
        method: 'post',
        data: data
    });
};
// 工作室取消理发师
export const cleanWorkRoomBarber = data => {
    return AjaxRequest.request({
        url: service_url+'/cleanWorkRoomBarber',
        method: 'post',
        data: data
    });
};
// 工作室取消服务价格项目
export const cleanWorkRoomBarberPrice = data => {
    return AjaxRequest.request({
        url: service_url+'/cleanWorkRoomBarberPrice',
        method: 'post',
        data: data
    });
};
// 工作室配置价格状态
export const configWorkRoomDisable = data => {
    return AjaxRequest.request({
        url: service_url+'/configWorkRoomDisable',
        method: 'post',
        data: data
    });
};