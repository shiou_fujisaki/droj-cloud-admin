import request from '../../utils/request';
import AjaxRequest from '../../utils/AjaxRequest';
/**
* Tools 请求工具
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// Barber 理发师 --------------------------------------------------------------------Start
const barber_url = "/admin/barber";
//  --------------------------------------------------------------------------------------------------------------- //
// 理发师列表分页查询
export const findBarberListPage = query => {
    return AjaxRequest.request({
        url: barber_url+'/findBarberListPage',
        method: 'get',
		params: query,
    });
};
// 理发师详情查询
export const findBarberInfoById = query => {
    return AjaxRequest.request({
        url: barber_url+'/findBarberInfoById/'+query,
        method: 'get',
		params: query,
    });
};
// 新增编辑理发师
export const addBarber = data => {
    return AjaxRequest.request({
        url: barber_url+'/saveBarber',
        method: 'post',
		data: data,
    });
};
// 新增编辑理发师
export const saveBarber = data => {
    return AjaxRequest.request({
        url: barber_url+'/saveBarber',
        method: 'put',
		data: data,
    });
};
// 删除理发师
export const deleteBarberById = data => {
    return AjaxRequest.request({
        url: barber_url+'/deleteBarberById/'+data,
        method: 'post',
		data: data,
    });
};