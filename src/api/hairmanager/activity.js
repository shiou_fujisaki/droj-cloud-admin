import request from '../../utils/request';
import AjaxRequest from '../../utils/AjaxRequest';
/**
* Tools 请求工具
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// Activity 营销积分活动 --------------------------------------------------------------------Start
const activity_url = "/admin/activity";
 //  --------------------------------------------------------------------------------------------------------------- //
// 营销积分活动列表分页查询
export const findActivityListPage = query => {
    return AjaxRequest.request({
        url: activity_url+'/findActivityListPage',
        method: 'get',
		params: query,
    });
};
// 营销积分活动详情查询
export const findActivityById = query => {
    return AjaxRequest.request({
        url: activity_url+'/findActivityById/'+query,
        method: 'get',
		params: query,
    });
};
// 新增营销积分活动
export const addActivity = data => {
    return AjaxRequest.request({
        url: activity_url+'/saveActivity',
        method: 'post',
		data: data,
    });
};
// 编辑营销积分活动
export const saveActivity = data => {
    return AjaxRequest.request({
        url: activity_url+'/saveActivity',
        method: 'put',
		data: data,
    });
};
// 删除营销积分活动
export const deleteActivityById = data => {
    return AjaxRequest.request({
        url: activity_url+'/deleteActivityById/'+data,
        method: 'delete',
    });
};

// 批量删除营销积分活动
export const batchDeleteActivity = data => {
    return AjaxRequest.request({
        url: activity_url+'/deleteActivity',
        method: 'delete',
        data: data
    });
};
// 营销积分活动状态批量更新
export const batchEditStatus = data => {
    return AjaxRequest.request({
        url: activity_url+'/batchEditStatus',
        method: 'put',
		data: data,
    });
};
// 新增编辑营销活动规则
export const saveActivityRuleInfo = data => {
    return AjaxRequest.request({
        url: activity_url+'/saveActivityRuleInfo',
        method: 'put',
		data: data,
    });
}
// 删除营销活动规则
export const deleteActivityRuleInfoById = data => {
    return AjaxRequest.request({
        url: activity_url+'/deleteActivityRuleInfoById/'+ data,
        method: 'delete',
    });
}