import request from '../../utils/request';
import AjaxRequest from '../../utils/AjaxRequest';
/**
 * Tools 请求工具
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// MemberAddress 会员收货地址 --------------------------------------------------------------------Start
const memberAddress_url = "/admin/memberAddress";
//  --------------------------------------------------------------------------------------------------------------- //
// 会员收货地址列表分页查询
export const findMemberAddressListPage = query => {
    return AjaxRequest.request({
        url: memberAddress_url+'/findMemberAddressListPage',
        method: 'get',
		params: query,
    });
};
// 会员收货地址列表查询
export const findMemberAddressList = query => {
    return AjaxRequest.request({
        url: memberAddress_url+'/findMemberAddressList',
        method: 'get',
		params: query,
    });
};
// 会员收货地址明细查询
export const findMemberAddressInfoById = query => {
    return AjaxRequest.request({
        url: memberAddress_url+'/findMemberAddressInfoById/'+query,
        method: 'get',
    });
};
// 添加会员收货地址
export const addMemberAddress = data => {
    return AjaxRequest.request({
        url: memberAddress_url+'/saveMemberAddress',
        method: 'post',
		data: data,
    });
};
// 编辑会员收货地址
export const saveMemberAddress = data => {
    return AjaxRequest.request({
        url: memberAddress_url+'/saveMemberAddress',
        method: 'put',
		data: data,
    });
};
// 删除会员收货地址
export const deleteMemberAddressById = data => {
    return AjaxRequest.request({
        url: memberAddress_url+'/deleteMemberAddressById/'+data,
        method: 'delete',
    });
};
// 批量删除会员收货地址
export const deleteMemberAddress = data => {
    return AjaxRequest.request({
        url: memberAddress_url+'/deleteMemberAddress',
        method: 'delete',
		data: data,
    });
};
// 编辑会员默认收件地址
export const modifyDefaultAddress = data => {
    return AjaxRequest.request({
        url: memberAddress_url+'/modifyDefaultAddress',
        method: 'put',
		params: data,
    });
};
