import request from '../../utils/request';
import AjaxRequest from '../../utils/AjaxRequest';
/**
 * Tools 请求工具
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// Commont 评论 --------------------------------------------------------------------Start
const commont_url = "/admin/commont";
//  --------------------------------------------------------------------------------------------------------------- //
// 评论列表分页查询
export const findCommontInfoListPage = query => {
    return AjaxRequest.request({
        url: commont_url+'/findCommontInfoListPage',
        method: 'get',
		params: query,
    });
};
// 评论详情查询
export const findCommontInfoListByDataID = query => {
    return AjaxRequest.request({
        url: commont_url+'/findCommontInfoListByDataID/'+query,
        method: 'get',
		params: query,
    });
};
// 新增评论
export const addCommontInfo = data => {
    return AjaxRequest.request({
        url: commont_url+'/saveCommontInfo',
        method: 'post',
		data: data,
    });
};
// 编辑评论
export const saveCommontInfo = data => {
    return AjaxRequest.request({
        url: commont_url+'/saveCommontInfo',
        method: 'put',
		data: data,
    });
};
// 删除评论
export const deleteCommontInfoById = data => {
    return AjaxRequest.request({
        url: commont_url+'/deleteCommontInfoById/'+data,
        method: 'delete',
    });
};
// 批量更新评论显示状态
export const batchEditIsShow = data => {
    return AjaxRequest.request({
        url: commont_url+'/batchEditIsShow',
        method: 'put',
		data: data,
    });
};
