import request from '../../utils/request';
import AjaxRequest from '../../utils/AjaxRequest';
/**
 * Tools 请求工具
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// MemberCoupon 会员优惠券 --------------------------------------------------------------------Start
const memberCoupon_url = "/admin/memberCoupon";
//  --------------------------------------------------------------------------------------------------------------- //
// 会员优惠券列表分页查询
export const findMemberCouponListPage = query => {
    return AjaxRequest.request({
        url: memberCoupon_url+'/findMemberCouponListPage',
        method: 'get',
		params: query,
    });
};
// 会员优惠券列表查询
export const findMemberCouponList = query => {
    return AjaxRequest.request({
        url: memberCoupon_url+'/findMemberCouponList',
        method: 'get',
		params: query,
    });
};
// 会员优惠券明细查询
export const findMemberCouponInfoById = query => {
    return AjaxRequest.request({
        url: memberCoupon_url+'/findMemberCouponInfoById/'+query,
        method: 'get',
    });
};
// 给指定用户分配优惠券
export const allocationCoupon = data => {
    return AjaxRequest.request({
        url: memberCoupon_url+'/allocationCoupon',
        method: 'put',
		data: data,
    });
};
// 删除会员优惠券
export const deleteMemberCouponById = data => {
    return AjaxRequest.request({
        url: memberCoupon_url+'/deleteMemberCouponById/'+data,
        method: 'delete',
    });
};
// 批量删除会员优惠券
export const deleteMemberCoupon = data => {
    return AjaxRequest.request({
        url: memberCoupon_url+'/deleteMemberCoupon',
        method: 'delete',
		data: data,
    });
};
