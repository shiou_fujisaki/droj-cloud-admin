import request from '../../utils/request';
import AjaxRequest from '../../utils/AjaxRequest';
/**
 * Tools 请求工具
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// Classify 商品分类 --------------------------------------------------------------------Start
const product_classify_url = "/admin/product/classify";
//  --------------------------------------------------------------------------------------------------------------- //
// 商品分类列表查询
export const findClassifyListPage = query => {
    return AjaxRequest.request({
        url: product_classify_url+'/findClassifyListPage',
        method: 'get',
		params: query
    });
};
// 商品分类详情查询
export const findClassifyInfoById = query => {
    return AjaxRequest.request({
        url: product_classify_url+'/findClassifyInfoById/'+query,
        method: 'get',
    });
};
// 商品分类关键词查询
export const findClassifyWithKeyword = query => {
    return AjaxRequest.request({
        url: product_classify_url+'/findClassifyWithKeyword',
        method: 'get',
		params: query
    });
};
// 商品子分类列表查询
export const findClassifyListChildPage = query => {
    return AjaxRequest.request({
        url: product_classify_url+'/findClassifyListChildPage',
        method: 'get',
		params: query
    });
};
// 商品分类下拉查询
export const findClassifyPullDownList = query => {
    return AjaxRequest.request({
        url: product_classify_url+'/findClassifyPullDownList',
        method: 'get',
		params: query
    });
};
// 新增商品分类
export const addClassify = data => {
    return AjaxRequest.request({
        url: product_classify_url+'/saveClassify',
        method: 'post',
		data: data
    });
};
// 编辑商品分类
export const saveClassify = data => {
    return AjaxRequest.request({
        url: product_classify_url+'/saveClassify',
        method: 'put',
		data: data
    });
};
// 删除商品分类
export const deleteClassify = data => {
    return AjaxRequest.request({
        url: product_classify_url+'/deleteClassify/'+data,
        method: 'delete',
    });
};
// 批量删除商品分类
export const batchDeleteClassify = data => {
    return AjaxRequest.request({
        url: product_classify_url+'/deleteClassify',
        method: 'delete',
        data: data
    });
};
