import request from '../../utils/request';
import AjaxRequest from '../../utils/AjaxRequest';
/**
 * Tools 请求工具
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// Attribute 分类属性信息 --------------------------------------------------------------------Start
const product_attribute_url = "/admin/product/attribute";
//  --------------------------------------------------------------------------------------------------------------- //
// 通过分类ID获取属性信息
export const findAttributeListPageByClassifyId = query => {
    return AjaxRequest.request({
        url: product_attribute_url+'/findAttributeListPageByClassifyId/',
        method: 'get',
        params: query,
    });
};
// 通过分类ID获取属性信息
export const findAttributeListByClassifyId = query => {
    return AjaxRequest.request({
        url: product_attribute_url+'/findAttributeListByClassifyId/'+query,
        method: 'get',
    });
};
// 通过分类ID和商品ID获取属性值信息
export const findAttributesByProductAndClassifyId = query => {
    return AjaxRequest.request({
        url: product_attribute_url+'/findAttributesByProductAndClassifyId',
        method: 'get',
        params: query,
    });
};
// 通过商品ID获取自定义规格信息
export const findSpecsByProductId = query => {
    return AjaxRequest.request({
        url: product_attribute_url+'/findSpecsByProductId',
        method: 'get',
        params: query,
    });
}
// 通过属性ID获取属性信息
export const findAttributeInfoById = query => {
    return AjaxRequest.request({
        url: product_attribute_url+'/findAttributeInfoById/' + query,
        method: 'get',
    });
};
// 新增属性
export const addAttribute = data => {
    return AjaxRequest.request({
        url: product_attribute_url+'/saveAttribute',
        method: 'post',
        data: data
    });
};
// 编辑属性
export const saveAttribute = data => {
    return AjaxRequest.request({
        url: product_attribute_url+'/saveAttribute',
        method: 'put',
        data: data
    });
};
// 删除属性
export const deleteAttribute = data => {
    return AjaxRequest.request({
        url: product_attribute_url+'/deleteAttribute/'+data,
        method: 'delete',
    });
};