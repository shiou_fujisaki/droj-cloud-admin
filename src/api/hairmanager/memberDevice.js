import request from '../../utils/request';
import AjaxRequest from '../../utils/AjaxRequest';
/**
 * Tools 请求工具
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// MemberDevice 会员设备 --------------------------------------------------------------------Start
const MemberDevice_url = "/admin/memberDevice";
//  --------------------------------------------------------------------------------------------------------------- //
// 会员设备列表分页查询
export const findMemberDeviceListPage = query => {
    return AjaxRequest.request({
        url: MemberDevice_url+'/findMemberDeviceListPage',
        method: 'get',
		params: query,
    });
};
// 会员设备详情查询
export const findMemberDeviceInfoById = query => {
    return AjaxRequest.request({
        url: MemberDevice_url+'/findMemberDeviceInfoById/'+query,
        method: 'get',
		params: query,
    });
};
// 新增会员设备
export const addMemberDevice = data => {
    return AjaxRequest.request({
        url: MemberDevice_url+'/saveMemberDevice',
        method: 'post',
		data: data,
    });
};
// 编辑会员设备
export const saveMemberDevice = data => {
    return AjaxRequest.request({
        url: MemberDevice_url+'/saveMemberDevice',
        method: 'put',
		data: data,
    });
};
// 删除会员设备
export const deleteMemberDeviceById = data => {
    return AjaxRequest.request({
        url: MemberDevice_url+'/deleteMemberDeviceById/'+data,
        method: 'delete',
    });
};
// 批量删除会员设备
export const batchDeleteMemberDevice = data => {
    return AjaxRequest.request({
        url: MemberDevice_url+'/deleteMemberDevice',
        method: 'delete',
        data: data
    });
};