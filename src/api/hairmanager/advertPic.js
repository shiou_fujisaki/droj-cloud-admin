import request from '../../utils/request';
import AjaxRequest from '../../utils/AjaxRequest';
/**
* Tools 请求工具
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// AdvertPic 广告位图片 --------------------------------------------------------------------Start
const advertPic_url = "/admin/advertPic";
//  --------------------------------------------------------------------------------------------------------------- //
// 广告位图片列表分页查询
export const findAdvertPicListPage = query => {
    return AjaxRequest.request({
        url: advertPic_url+'/findAdvertPicListPage',
        method: 'get',
		params: query,
    });
};
// 广告位图片详情查询
export const findAdvertPicInfoById = query => {
    return AjaxRequest.request({
        url: advertPic_url+'/findAdvertPicInfoById/'+query,
        method: 'get',
		params: query,
    });
};
// 新增广告位图片
export const addAdvertPic = data => {
    return AjaxRequest.request({
        url: advertPic_url+'/saveAdvertPic',
        method: 'post',
		data: data,
    });
};
// 编辑广告位图片
export const saveAdvertPic = data => {
    return AjaxRequest.request({
        url: advertPic_url+'/saveAdvertPic',
        method: 'put',
		data: data,
    });
};
// 删除广告位图片
export const deleteAdvertPic = data => {
    return AjaxRequest.request({
        url: advertPic_url+'/deleteAdvertPic/'+data,
        method: 'delete',
    });
};
// 批量删除广告位图片
export const batchDeleteAdvertPic = data => {
    return AjaxRequest.request({
        url: advertPic_url+'/batchDeleteAdvertPic',
        method: 'delete',
        data: data
    });
};
// 广告位图片状态批量更新
export const batchEditStatus = data => {
    return AjaxRequest.request({
        url: advertPic_url+'/batchEditStatus',
        method: 'put',
		data: data,
    });
};