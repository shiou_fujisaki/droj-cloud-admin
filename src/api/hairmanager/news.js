import request from '../../utils/request';
import AjaxRequest from '../../utils/AjaxRequest';
/**
 * Tools 请求工具
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// News 会员分享话题 --------------------------------------------------------------------Start
const news_url = "/admin/news";
//  --------------------------------------------------------------------------------------------------------------- //
// 新闻资讯列表分页查询
export const findNewsListPage = query => {
    return AjaxRequest.request({
        url: news_url+'/findNewsListPage',
        method: 'get',
		params: query,
    });
};
// 新闻资讯列表查询
export const findNewsList = query => {
    return AjaxRequest.request({
        url: news_url+'/findNewsList',
        method: 'get',
		params: query,
    });
};
// 新闻资讯详情查询
export const findNewsInfoById = query => {
    return AjaxRequest.request({
        url: news_url+'/findNewsInfoById/'+query,
        method: 'get',
        
		params: query,
    });
};
// 新增新闻资讯
export const addNews = data => {
    return AjaxRequest.request({
        url: news_url+'/saveNews',
        method: 'post',
		data: data,
    });
};
// 编辑新闻资讯
export const saveNews = data => {
    return AjaxRequest.request({
        url: news_url+'/saveNews',
        method: 'put',
		data: data,
    });
};
// 删除新闻资讯
export const deleteNews = data => {
    return AjaxRequest.request({
        url: news_url+'/deleteNews/'+data,
        method: 'delete',
    });
};
// 删除新闻资讯
export const batchDeleteNews = data => {
    return AjaxRequest.request({
        url: news_url+'/deleteNews',
        method: 'delete',
        data: data
    });
};
// 新闻资讯状态批量更新
export const batchEditStatus = data => {
    return AjaxRequest.request({
        url: news_url+'/batchEditStatus',
        method: 'put',
		data: data,
    });
};