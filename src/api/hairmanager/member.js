import request from '../../utils/request';
import AjaxRequest from '../../utils/AjaxRequest';
/**
 * Tools 请求工具
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// Member 会员 --------------------------------------------------------------------Start
const member_url = "/admin/member";
//  --------------------------------------------------------------------------------------------------------------- //
// 会员列表分页查询
export const findMemberListPageByLikeNickName = query => {
    return AjaxRequest.request({
        url: member_url+'/findMemberListPageByLikeNickName',
        method: 'get',
		params: query,
    });
};
// 会员列表查询
export const findMemberList = query => {
    return AjaxRequest.request({
        url: member_url+'/findMemberList',
        method: 'get',
		params: query,
    });
};
// 会员明细查询
export const findMemberInfoById = query => {
    return AjaxRequest.request({
        url: member_url+'/findMemberInfoById/'+query,
        method: 'get',
    });
};
// 添加会员账户
export const addMemberInfo = data => {
    return AjaxRequest.request({
        url: member_url+'/saveMemberInfo',
        method: 'post',
		data: data,
    });
};
// 编辑会员账户
export const saveMemberInfo = data => {
    return AjaxRequest.request({
        url: member_url+'/saveMemberInfo',
        method: 'put',
		data: data,
    });
};
// 批量更新会员状态
export const batchEditMemberStatus = data => {
    return AjaxRequest.request({
        url: member_url+'/batchEditMemberStatus',
        method: 'put',
		data: data,
    });
};
// 批量删除会员
export const deleteMember = data => {
    return AjaxRequest.request({
        url: member_url+'/deleteMember',
        method: 'delete',
		data: data,
    });
};
// 删除会员
export const deleteMemberById = data => {
    return AjaxRequest.request({
        url: member_url+'/deleteMemberById/data',
        method: 'delete',
    });
};
