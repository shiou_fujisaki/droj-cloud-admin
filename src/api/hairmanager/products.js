import request from '../../utils/request';
import AjaxRequest from '../../utils/AjaxRequest';
/**
 * Tools 请求工具
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// Product 商品 --------------------------------------------------------------------Start
const product_url = "/admin/product";
//  --------------------------------------------------------------------------------------------------------------- //
// 商品分页列表查询
export const findProductInfoListPage = query => {
    return AjaxRequest.request({
        url: product_url+'/findProductInfoListPage',
        method: 'get',
		params: query
    });
};
// 商品详情查询
export const findProductInfoListById = query => {
    return AjaxRequest.request({
        url: product_url+'/findProductInfoListById/'+query,
        method: 'get',
    });
};
// 新增编辑商品信息
export const addProductInfo = data => {
    return AjaxRequest.request({
        url: product_url+'/saveProductInfo',
        method: 'post',
		data: data,
	});
};
// 新增编辑商品信息
export const saveProductInfo = data => {
    return AjaxRequest.request({
        url: product_url+'/saveProductInfo',
        method: 'put',
		data: data,
	});
};
// 删除商品信息
export const deleteProductById = query => {
    return AjaxRequest.request({
        url: product_url+'/deleteProductInfoById/'+query,
        method: 'delete',
    });
};
// 批量更新商品发布状态
export const batchEditProductPublish = data => {
    return AjaxRequest.request({
        url: product_url+'/batchEditProductPublish',
        method: 'put',
        data: data
    });
};

// 商品回收站
export const findDropProductInfoListPage = param => {
    return AjaxRequest.request({
        url: product_url+'/findDropProductInfoListPage',
        method: 'get',
        params: param
    });
};
// 恢复商品
export const recoveryProduct = data => {
    return AjaxRequest.request({
        url: product_url+'/recoveryProduct',
        method: 'put',
        data: data
    });
};
//  --------------------------------------------------------------------------------------------------------------- //
// 新增编辑商品属性
export const saveProductAttrInfo = data => {
    return AjaxRequest.request({
        url: product_url+'/saveProductAttrInfo',
        method: 'put',
		data: data,
    });
};
// 新增编辑商品规格
export const saveProductSpecInfo = data => {
    return AjaxRequest.request({
        url: product_url+'/saveProductSpecInfo',
        method: 'put',
		data: data,
    });
};
// 删除商品规格
export const deleteProductSpecInfoById = query => {
    return AjaxRequest.request({
        url: product_url+'/deleteProductSpecInfoById/'+query,
        method: 'delete',
    });
};