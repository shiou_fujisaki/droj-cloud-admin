import request from '../../utils/request';
import AjaxRequest from '../../utils/AjaxRequest';
/**
 * Tools 请求工具
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// MemberSubject 会员分享话题 --------------------------------------------------------------------Start
const memberSubject_url = "/admin/memberSubject";
//  --------------------------------------------------------------------------------------------------------------- //
//会员分享话题列表分页查询
export const findMemberSubjectListPage = query => {
    return AjaxRequest.request({
        url: memberSubject_url+'/findMemberSubjectListPage',
        method: 'get',
		params: query,
    });
};
//会员分享话题详情查询
export const findMemberSubjectInfoById = query => {
    return AjaxRequest.request({
        url: memberSubject_url+'/findMemberSubjectInfoById/'+query,
        method: 'get',
		params: query,
    });
};
// 新增会员分享话题
export const addMemberSubject = data => {
    return AjaxRequest.request({
        url: memberSubject_url+'/saveMemberSubject',
        method: 'post',
		data: data,
    });
};
// 编辑会员分享话题
export const saveMemberSubject = data => {
    return AjaxRequest.request({
        url: memberSubject_url+'/saveMemberSubject',
        method: 'put',
		data: data,
    });
};
// 删除会员分享话题
export const deleteMemberSubjectById = data => {
    return AjaxRequest.request({
        url: memberSubject_url+'/deleteMemberSubjectById/'+data,
        method: 'delete',
    });
};
// 批量删除会员分享话题
export const batchDeleteMemberSubject = data => {
    return AjaxRequest.request({
        url: memberSubject_url+'/deleteMemberSubject',
        method: 'delete',
        data: data
    });
};
// 会员分享话题留言
export const reBackMemberSubject = data => {
    return AjaxRequest.request({
        url: memberSubject_url+'/reBackMemberSubject',
        method: 'put',
        data: data
    });
};
// 删除会员分享话题留言
export const delMemberSubjectCommon = data => {
    return AjaxRequest.request({
        url: memberSubject_url+'/delMemberSubjectCommon',
        method: 'delete',
        data: data
    });
};

