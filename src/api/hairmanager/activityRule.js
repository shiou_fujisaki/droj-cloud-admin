import request from '../../utils/request';
import AjaxRequest from '../../utils/AjaxRequest';
/**
* Tools 请求工具
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// ActivityRule 营销积分活动规则 --------------------------------------------------------------------Start
const activityRule_url = "/admin/activityRule";
//  --------------------------------------------------------------------------------------------------------------- //
// 营销积分活动列表分页查询
export const findActivityRuleListPage = query => {
    return AjaxRequest.request({
        url: activityRule_url+'/findActivityRuleListPage',
        method: 'get',
		params: query,
    });
};
// 营销积分活动列表分页查询
export const findActivityRuleListByActId = query => {
    return AjaxRequest.request({
        url: activityRule_url+'/findActivityRuleInfoListByActivityId'+query,
        method: 'get',
    });
};
// 营销积分活动详情查询
export const findActivityRuleInfoById = query => {
    return AjaxRequest.request({
        url: activityRule_url+'/findActivityRuleInfoById/'+query,
        method: 'get',
		params: query,
    });
};
// 新增编辑营销活动规则
export const addActivityRuleInfo = data => {
    return AjaxRequest.request({
        url: activityRule_url+'/saveActivityRuleInfo',
        method: 'post',
		data: data,
    });
};
// 新增编辑营销活动规则
export const saveActivityRuleInfo = data => {
    return AjaxRequest.request({
        url: activityRule_url+'/saveActivityRuleInfo',
        method: 'put',
		data: data,
    });
};
// 删除营销活动规则
export const deleteActivityRule = data => {
    return AjaxRequest.request({
        url: activityRule_url+'/deleteActivityRuleInfoById/'+ data,
        method: 'delete',
    });
};
// 批量删除营销活动规则
export const batchDeleteActivityRule = data => {
    return AjaxRequest.request({
        url: activityRule_url+'/deleteActivityRule',
        method: 'delete',
        data: data
    });
};
// 营销活动规则状态批量更新
export const batchEditStatus = data => {
    return AjaxRequest.request({
        url: activityRule_url+'/batchEditStatus',
        method: 'put',
		data: data,
    });
};
