import request from '../../utils/request';
import AjaxRequest from '../../utils/AjaxRequest';
/**
 * Tools 请求工具
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// News tag 新闻资讯标签 --------------------------------------------------------------------Start
const news_tag_url = "/admin/news/tag";
//  --------------------------------------------------------------------------------------------------------------- //
// 新闻资讯标签查询
export const findNewsTagList = query => {
    return AjaxRequest.request({
        url: news_tag_url+'/findNewsTagList',
        method: 'get',
		params: query,
    });
};
// 新闻资讯标签列表分页查询
export const findNewsTagListPage = query => {
    return AjaxRequest.request({
        url: news_tag_url+'/findNewsTagListPage',
        method: 'get',
		params: query,
    });
};
// 新闻资讯标签详情查询
export const findNewsTagInfoById = query => {
    return AjaxRequest.request({
        url: news_tag_url+'/findNewsTagInfoById/'+query,
        method: 'get',
		params: query,
    });
};
// 新增编辑新闻资讯标签
export const addNewsTag = data => {
    return AjaxRequest.request({
        url: news_tag_url+'/saveNewsTag',
        method: 'post',
		data: data,
    });
};
// 新增编辑新闻资讯标签
export const saveNewsTag = data => {
    return AjaxRequest.request({
        url: news_tag_url+'/saveNewsTag',
        method: 'put',
		data: data,
    });
};
// 删除新闻资讯标签
export const deleteNewsTag = data => {
    return AjaxRequest.request({
        url: news_tag_url+'/deleteNewsTag/'+data,
        method: 'delete',
    });
};
// 批量删除新闻资讯标签
export const batchDeleteNewsTag = data => {
    return AjaxRequest.request({
        url: news_tag_url+'/deleteNewsTag',
        method: 'delete',
		data: data,
    });
};
