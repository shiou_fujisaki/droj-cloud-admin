import request from '../../utils/request';
import AjaxRequest from '../../utils/AjaxRequest';
/**
 * Tools 请求工具
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// MemberIntel 会员积分 --------------------------------------------------------------------Start
const memberIntel_url = "/admin/memberIntel";
//  --------------------------------------------------------------------------------------------------------------- //
//会员积分列表分页查询
export const findMemberIntelListPage = query => {
    return AjaxRequest.request({
        url: memberIntel_url+'/findMemberIntelListPage',
        method: 'get',
		params: query,
    });
};
//会员积分详情查询
export const findMemberIntelInfoById = query => {
    return AjaxRequest.request({
        url: memberIntel_url+'/findMemberIntelInfoById/'+query,
        method: 'get',
		params: query,
    });
};
// 新增会员积分
export const addMemberIntel = data => {
    return AjaxRequest.request({
        url: memberIntel_url+'/saveMemberIntel',
        method: 'post',
		data: data,
    });
};
// 编辑会员积分
export const saveMemberIntel = data => {
    return AjaxRequest.request({
        url: memberIntel_url+'/saveMemberIntel',
        method: 'put',
		data: data,
    });
};
// 删除会员积分
export const deleteMemberIntelById = data => {
    return AjaxRequest.request({
        url: memberIntel_url+'/deleteMemberIntelById/'+data,
        method: 'delete',
    });
};
// 批量删除会员积分
export const batchDeleteMemberIntel = data => {
    return AjaxRequest.request({
        url: memberIntel_url+'/deleteMemberIntel',
        method: 'delete',
        data: data
    });
};
//会员历史积分详情查询
export const findMemberIntelListByMemberId = query => {
    return AjaxRequest.request({
        url: memberIntel_url+'/findMemberIntelListByMemberId',
        method: 'get',
		params: query,
    });
};
