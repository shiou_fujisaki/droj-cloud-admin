import request from '../../utils/request';
import AjaxRequest from '../../utils/AjaxRequest';
/**
 * Tools 请求工具
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// Service 服务项目 --------------------------------------------------------------------Start
const service_url = "/admin/service";
//  --------------------------------------------------------------------------------------------------------------- //
// 服务项目树型列表查询
export const fetchProjectTreeData = query => {
    return AjaxRequest.request({
        url: service_url+'/findServiceProjectTreeList',
        method: 'get',
    });
};
// 服务项目列表查询
export const findActiveServiceProjectList = query => {
    return AjaxRequest.request({
        url: service_url+'/findActiveServiceProjectListPage',
        method: 'get',
        params: query,
    });
};
export const findServiceProjectListPage = query => {
    return AjaxRequest.request({
        url: service_url+'/findServiceProjectListPage',
        method: 'get',
        params: query,
    });
};  
// 服务项目详情查询
export const findServiceProjectById = query => {
    return AjaxRequest.request({
        url: service_url+'/findServiceProjectById/'+query,
        method: 'get',
    });
};
// 获取服务项目的父级列表信息
export const findServiceProjectParentList = query => {
    return AjaxRequest.request({
        url: service_url+'/findServiceProjectParentList',
        method: 'get',
    });
};
// 服务项目配置启用状态
export const configServiceProjectDisable = data => {
    return AjaxRequest.request({
        url: service_url+'/configServiceProjectDisable',
        method: 'post',
        data: data
    });
};

// 新增编辑服务项目
export const saveServiceProject = data => {
    return AjaxRequest.request({
        url: service_url+'/saveServiceProject',
        method: 'post',
        data: data
    });
};
// 删除服务项目
export const deleteServiceProjectById = data => {
    return AjaxRequest.request({
        url: service_url+'/deleteServiceProjectById/'+data,
        method: 'post',
        data: data
    });
};