import request from '../../utils/request';
import AjaxRequest from '../../utils/AjaxRequest';
/**
 * Tools 请求工具
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// Coupon 优惠券 --------------------------------------------------------------------Start
const coupon_url = "/admin/coupon";
//  --------------------------------------------------------------------------------------------------------------- //
// 优惠券列表分页查询
export const findCouponListPage = query => {
    return AjaxRequest.request({
        url: coupon_url+'/findCouponListPage',
        method: 'get',
		params: query,
    });
};
// 优惠券详情查询
export const findCouponInfoById = query => {
    return AjaxRequest.request({
        url: coupon_url+'/info/'+query,
        method: 'get',
		params: query,
    });
};
// 新增优惠券
export const addCoupon = data => {
    return AjaxRequest.request({
        url: coupon_url+'/saveCouponInfo',
        method: 'post',
		data: data,
    });
};
// 编辑优惠券
export const saveCoupon = data => {
    return AjaxRequest.request({
        url: coupon_url+'/saveCouponInfo',
        method: 'put',
		data: data,
    });
};
// 删除优惠券
export const deleteCouponById = data => {
    return AjaxRequest.request({
        url: coupon_url+'/deleteCouponById/'+data,
        method: 'delete',
    });
};
// 批量删除优惠券
export const batchDeleteCoupon = data => {
    return AjaxRequest.request({
        url: coupon_url+'/deleteCoupon',
        method: 'delete',
        data: data
    });
};
// 批量编辑优惠券状态
export const batchEditCouponStatus = data => {
    return AjaxRequest.request({
        url: coupon_url+'/batchEditCouponStatus',
        method: 'put',
        data: data
    });
};
// 优惠券分配
export const allocationCoupon = data => {
    return AjaxRequest.request({
        url: coupon_url+'/allocationCoupon',
        method: 'put',
        data: data
    });
};