import request from '../../utils/request';
import AjaxRequest from '../../utils/AjaxRequest';
/**
 * Tools 请求工具
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// News catalog 新闻资讯分类 --------------------------------------------------------------------Start
const news_catalog_url = "/admin/news/catalog";
//  --------------------------------------------------------------------------------------------------------------- //
// 新闻资讯分类查询
export const findNewsCatalogList = query => {
    return AjaxRequest.request({
        url: news_catalog_url+'/findNewsCatalogList',
        method: 'get',
		params: query,
    });
};
// 新闻资讯栏目列表分页查询
export const findNewsCatalogListPage = query => {
    return AjaxRequest.request({
        url: news_catalog_url+'/findNewsCatalogListPage',
        method: 'get',
		params: query,
    });
};
// 新闻资讯栏目详情查询
export const findNewsCatalogInfoById = query => {
    return AjaxRequest.request({
        url: news_catalog_url+'/findNewsCatalogInfoById/'+query,
        method: 'get',
		params: query,
    });
};
// 新增编辑新闻资讯栏目
export const addNewsCatalog = data => {
    return AjaxRequest.request({
        url: news_catalog_url+'/saveNewsCatalog',
        method: 'post',
		data: data,
    });
};
// 新增编辑新闻资讯栏目
export const saveNewsCatalog = data => {
    return AjaxRequest.request({
        url: news_catalog_url+'/saveNewsCatalog',
        method: 'put',
		data: data,
    });
};
// 删除新闻资讯栏目
export const deleteNewsCatalog = data => {
    return AjaxRequest.request({
        url: news_catalog_url+'/deleteNewsCatalog/'+data,
        method: 'delete',
    });
};
// 批量删除新闻资讯栏目
export const batchDeleteNewsCatalog = data => {
    return AjaxRequest.request({
        url: news_catalog_url+'/deleteNewsCatalog',
        method: 'delete',
		data: data,
    });
};
// 批量修改数据显示状态
export const batchIsShow = data => {
    return AjaxRequest.request({
        url: news_catalog_url+'/batchIsShow',
        method: 'put',
		data: data,
    });
};
// 批量修改数据独立显示状态
export const batchIsSplit = data => {
    return AjaxRequest.request({
        url: news_catalog_url+'/batchIsSplit',
        method: 'put',
		data: data,
    });
};
// 设定唯一栏目
export const setOnlyFlg = data => {
    return AjaxRequest.request({
        url: news_catalog_url+'/setOnlyFlg',
        method: 'put',
		data: data,
    });
};
// 读取唯一栏目
export const getOnlyCatalog = () => {
    return AjaxRequest.request({
        url: news_catalog_url+'/getOnlyCatalog',
        method: 'get',
    });
};
