import request from '../../utils/request';
import AjaxRequest from '../../utils/AjaxRequest';
/**
 * Tools 请求工具
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// MemberGrade 会员等级 --------------------------------------------------------------------Start
const memberGrade_url = "/admin/memberGrade";
//  --------------------------------------------------------------------------------------------------------------- //
// 会员等级列表分页查询
export const findMemberGradeListPage = query => {
    return AjaxRequest.request({
        url: memberGrade_url+'/findMemberGradeListPage',
        method: 'get',
		params: query,
    });
};
// 会员等级详情查询
export const findMemberGradeInfoById = query => {
    return AjaxRequest.request({
        url: memberGrade_url+'/findMemberGradeInfoById/'+query,
        method: 'get',
		params: query,
    });
};
// 新增会员等级
export const addMemberGrade = data => {
    return AjaxRequest.request({
        url: memberGrade_url+'/saveMemberGrade',
        method: 'post',
		data: data,
    });
};
// 编辑会员等级
export const saveMemberGrade = data => {
    return AjaxRequest.request({
        url: memberGrade_url+'/saveMemberGrade',
        method: 'put',
		data: data,
    });
};
// 删除会员等级
export const deleteMemberGradeById = data => {
    return AjaxRequest.request({
        url: memberGrade_url+'/deleteMemberGradeById/'+data,
        method: 'delete',
    });
};
// 批量删除会员等级
export const batchDeleteMemberGrade = data => {
    return AjaxRequest.request({
        url: memberGrade_url+'/deleteMemberGrade',
        method: 'delete',
        data: data
    });
};
// 批量更新会员等级折扣
export const updateGradeDis = data => {
    return AjaxRequest.request({
        url: memberGrade_url+'/updateGradeDis',
        method: 'put',
        data: data
    });
}