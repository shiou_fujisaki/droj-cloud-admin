import request from '../../utils/request';
import AjaxRequest from '../../utils/AjaxRequest';
/**
 * Tools 请求工具
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// Board 营销积分活动 --------------------------------------------------------------------Start
const board_url = "/admin/board";
//  --------------------------------------------------------------------------------------------------------------- //
// 品牌列表分页查询
export const findBoardInfoListPage = query => {
    return AjaxRequest.request({
        url: board_url+'/findBoardInfoListPage',
        method: 'get',
		params: query,
    });
};
// 品牌详情查询
export const findBoardInfoListById = query => {
    return AjaxRequest.request({
        url: board_url+'/findBoardInfoListById/'+query,
        method: 'get',
		params: query,
    });
};
// 新增品牌
export const addBoardInfo = data => {
    return AjaxRequest.request({
        url: board_url+'/saveBoardInfo',
        method: 'post',
		data: data,
    });
};
// 编辑品牌
export const saveBoardInfo = data => {
    return AjaxRequest.request({
        url: board_url+'/saveBoardInfo',
        method: 'put',
		data: data,
    });
};
// 删除品牌
export const deleteBoardInfoById = data => {
    return AjaxRequest.request({
        url: board_url+'/deleteBoardInfoById/'+data,
        method: 'delete',
		data: data,
    });
};