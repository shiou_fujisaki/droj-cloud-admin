import request from '../../utils/request';
import AjaxRequest from '../../utils/AjaxRequest';
/**
 * Tools 请求工具
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// AttributeValue 分类属性值信息 --------------------------------------------------------------------Start
const product_attributeValue_url = "/admin/product/attributeValue";
//  --------------------------------------------------------------------------------------------------------------- //
// 新增修改分类多条属性值
export const findAttributeValueListPage = query => {
    return AjaxRequest.request({
        url: product_attributeValue_url+'/findAttributeValueListPage/',
        method: 'get',
		params: query,
    });
};
// 新增修改分类多条属性值
export const findAttributeValueListByAttrId = query => {
    return AjaxRequest.request({
        url: product_attributeValue_url+'/findAttributeValueListByAttrId/',
        method: 'get',
		params: query,
    });
};
// 新增修改分类多条属性值
export const findAttributeValueInfoById = query => {
    return AjaxRequest.request({
        url: product_attributeValue_url+'/findAttributeValueInfoById/' + query,
        method: 'get',
    });
};
// 新增分类属性值
export const addAttributeValue = data => {
    return AjaxRequest.request({
        url: product_attributeValue_url+'/saveAttributeValue/',
        method: 'post',
		data: data,
    });
};
// 编辑分类属性值
export const saveAttributeValue = data => {
    return AjaxRequest.request({
        url: product_attributeValue_url+'/saveAttributeValue/',
        method: 'put',
		data: data,
    });
};
// 删除分类属性值
export const deleteAttributeValue = data => {
    return AjaxRequest.request({
        url: product_attributeValue_url+'/deleteAttributeValue/'+data,
        method: 'delete',
    });
};
// 新增修改分类多条属性值
export const batchSaveAttributeValueList = data => {
    return AjaxRequest.request({
        url: product_attributeValue_url+'/batchSaveAttributeValueList/',
        method: 'post',
		data: data,
    });
};
