import request from '../../utils/request';
let baseRequest_url = "http://47.100.167.68:8084/zcms";
//  --------------------------------------------------------------------------------------------------------------- //
/**
 * Tools 公共工具
 * 读取Excel模板数据
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
// LoadExcel Excel文件管理 --------------------------------------------------------------------Start
export const loadExcelSheet = query => {
    return request({
        url: baseRequest_url+'/mailEvent/email/customer/getSheetData',
        method: 'post',
        params: query
    });
};