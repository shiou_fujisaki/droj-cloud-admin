import request from '../utils/request';
let baseRequest_url = "http://47.100.167.68:8084/zcms";
//  --------------------------------------------------------------------------------------------------------------- //
/**
 * 斯普瑞 大乐透工具
 * 读取Excel模板数据
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
//  --------------------------------------------------------------------------------------------------------------- //
// 中奖记录
export const fetchLottoData = query => {
    return request({
        url: baseRequest_url+'/lotto/rafflelist',
        method: 'get',
        params: query
    });
};
//  --------------------------------------------------------------------------------------------------------------- //
// 抽奖活动
export const fetchLottoEventData = query => {
    return request({
        url: baseRequest_url+'/event/eventlist',
        method: 'get',
        params: query
    });
};

export const addLottoEventData = data => {
    return request({
        url: baseRequest_url+'/event/add',
        method: 'post',
        data: data
    });
};

export const editLottoEventData = (id,data) => {
    return request({
        url: baseRequest_url+'/event/edit?id=' + id,
        method: 'post',
        data: data
    });
};
export const delLottoEventData = (id) => {
    return request({
        url: baseRequest_url+'/event/del?id=' + id,
        method: 'get'
    });
};
export const batchDelLottoEventData = (data) => {
    return request({
        url: baseRequest_url+'/event/batchDel',
        method: 'post',
        data: data
    });
};
// 分配抽奖活动奖池
export const disPrizes = (id,data) => {
    return request({
        url: baseRequest_url+'/event/disPrizes?id=' + id,
        method: 'post',
        data: data
    });
};
//  --------------------------------------------------------------------------------------------------------------- //
// 奖品信息下拉列表
export const getLottoSelectDatas = query => {
    return request({
        url: baseRequest_url+'/lotto/lottoSelectList',
        method: 'get',
        params: query
    });
};
// 奖品信息
export const fetchLottoData = query => {
    return request({
        url: baseRequest_url+'/lotto/prizelist',
        method: 'get',
        params: query
    });
};
export const getLottoData = query => {
    return request({
        url: baseRequest_url+'/lotto/info',
        method: 'get',
        params: query
    });
};
export const delLottoData = query => {
    return request({
        url: baseRequest_url+'/lotto/del',
        method: 'get',
        params: query
    });
};
export const batchDelLottoData = data => {
    return request({
        url: baseRequest_url+'/lotto/batchDel',
        method: 'post',
        data: data
    });
};
export const addLottoData = data => {
    return request({
        url: baseRequest_url+'/lotto/add',
        method: 'post',
        data: data
    });
};
export const editLottoData = (id,data) => {
    return request({
        url: baseRequest_url+'/lotto/edit?id=' + id,
        method: 'post',
        data: data
    });
};
