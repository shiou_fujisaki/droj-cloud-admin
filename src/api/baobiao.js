import request from '../utils/request';
import AjaxRequest from '../utils/AjaxRequest';
/**
 * Tools 公共工具
 * 字典模块API资源文件
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
 // Report 首页报表数据 --------------------------------------------------------------------Start
const baobiao_url = "/admin/baobiao";
//  --------------------------------------------------------------------------------------------------------------- //
 // 平台数据总量查询
export const baobiao1 = query => {
    return AjaxRequest.request({
        url: baobiao_url+'/a',
        method: 'get',
		params: query,
    });
};
// 饼图 注册用户数量 有购买用户的数量
export const baobiao2 = query => {
    return AjaxRequest.request({
        url: baobiao_url+'/b',
        method: 'get',
		params: query,
    });
};
// 饼图 用户等级分布
export const baobiao3 = query => {
    return AjaxRequest.request({
        url: baobiao_url+'/c',
        method: 'get',
		params: query,
    });
};
// 系列柱状图 6个月内每月赠送积分 及 消费积分
export const baobiao4 = query => {
    return AjaxRequest.request({
        url: baobiao_url+'/d',
        method: 'get',
		params: query,
    });
};
// 系列柱状图 30天内每天新增用户 及 6个月内每月的新增用户
export const baobiao5 = query => {
    return AjaxRequest.request({
        url: baobiao_url+'/e',
        method: 'get',
		params: query,
    });
};
