import request from '../utils/request';
import AjaxRequest from '../utils/AjaxRequest';
/**
 * Tools 公共工具
 * 字典模块API资源文件
 * 
 * @author 小兔AjaxRequest.request
 * @Date 20210905
 * @description
 */
//  var baseRequest_url = "http://tuut.huoshangyun.com/portal/admin";
// Dictionary 字典管理 --------------------------------------------------------------------Start
export const fetchDictionaryById  = query => {
    return AjaxRequest.request({
        url: '/admin/dic/info/'+query,
        method: 'get'
    })
}
// 新增字典
export const addDictionary = data => {
    return AjaxRequest.request({
        url: '/admin/dic/saveDictionary',
        method: 'post',
		data: data
    });
};
// 编辑字典
export const saveDictionary = data => {
    return AjaxRequest.request({
        url: '/admin/dic/saveDictionary',
        method: 'put',
		data: data
    });
};
// 标识字典 删除状态
export const editDictionaryDel = data => {
    return AjaxRequest.request({
        url: '/admin/dic/editDictionaryDel',
        method: 'delete',
		data: data
    });
};
// 删除字典
export const delDictionary = data => {
    return AjaxRequest.request({
        url: '/admin/dic/delDictionary',
        method: 'delete',
		data: data
    });
};


// 获取所有父级字典
export const fetchParentDictionaryList = query => {
    return AjaxRequest.request({
        url: '/admin/dic/findParentDictionaryList',
        method: 'get',
        params:query
    });
};

// 通过父级id获取子级字典信息
export const fetchChildsListByParentId = query => {
    return AjaxRequest.request({
        url: '/admin/dic/findChildDictionaryListByParentId',
        method: 'get',
		params: query,
    });
};

// 通过父级编码获取字典信息
export const findDictionaryInfoByDicCode = query => {
    return AjaxRequest.request({
        url: '/admin/dic/findDictionaryInfoByDicCode',
        method: 'get',
		params: query,
    });
};
 // Dictionary 系统公共Code --------------------------------------------------------------------Start
// 新闻类型字典
export const loadNewsTypeDict = query => {
    return AjaxRequest.request({
        url: '/admin/status/view/NewsTypeEnum',
        method: 'get',
		params: query,
    });
};
// 新闻阅读权限字典
export const loadNewsReadPermissionsDict = query => {
    return AjaxRequest.request({
        url: '/admin/status/view/NewsReadPermissions',
        method: 'get',
		params: query,
    });
};

// 广告位展示方式字典
export const loadAdvertOpenWayDict = query => {
    return AjaxRequest.request({
        url: '/admin/status/view/AdvertOpenWayEnum',
        method: 'get',
		params: query,
    });
};

// 广告位展示位置字典
export const loadAdvertPositionDict = query => {
    return AjaxRequest.request({
        url: '/admin/status/view/AdvertPositionEnum',
        method: 'get',
		params: query,
    });
};

// 产品发布状态字典
export const loadProductPublishStatusDict = query => {
    return AjaxRequest.request({
        url: '/admin/status/view/productPublishStatus',
        method: 'get',
		params: query,
    });
};

// 会员等级晋升方式
export const loadMemberGradeUpWay = query => {
    return AjaxRequest.request({
        url: '/admin/status/view/MemberGradeUpWayEnum',
        method: 'get',
		params: query,
    });
};

// 产品发布状态字典
export const loadProvTrees = query => {
    return AjaxRequest.request({
        url: '/childFetch',
        method: 'get',
		params: query,
    });
};

// 会员积分来源
export const loadIntegralWayEnum = query => {
    return AjaxRequest.request({
        url: '/admin/status/view/IntegralWayEnum',
        method: 'get',
		params: query,
    });
};

// 话题类型
export const loadSubjectCommonTypeEnum = query => {
    return AjaxRequest.request({
        url: '/admin/status/view/SubjectCommonTypeEnum',
        method: 'get',
		params: query,
    });
};

// 优惠券类型
export const loadCouponTypeEnum = query => {
    return AjaxRequest.request({
        url: '/admin/status/view/CouponTypeEnum',
        method: 'get',
		params: query,
    });
};

// 优惠券使用用途
export const loadCouponUseEnum = query => {
    return AjaxRequest.request({
        url: '/admin/status/view/CouponUseEnum',
        method: 'get',
		params: query,
    });
};