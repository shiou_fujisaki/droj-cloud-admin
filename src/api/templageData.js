import request from '../utils/request';
import AjaxRequest from '../utils/AjaxRequest';
//  --------------------------------------------------------------------------------------------------------------- //
/**
 * 文件数据模板
 * @param {*} data 
 * @returns 
 */
export const dataTemplet = param => {
    return AjaxRequest.request({
        url: '/dataExcel/dataTemplet',
        method: 'get',
        params: param,
    });
};

/**
 * 会员信息导入
 * @param {*} data 
 * @returns 
 */
export const memberDataLeadIn = data => {
    return AjaxRequest.request({
        url: '/dataExcel/memberDataLeadIn',
        method: 'post',
        data: data,
    });
};

/**
 * 会员信息导出
 * @param {*} data 
 * @returns 
 */
export const memberDataLeadOut = data => {
    outFile(data)
};

/**
 * 简要商品信息导入
 * @param {*} data 
 * @returns 
 */
 export const productDataLeadIn = data => {
    return AjaxRequest.request({
        url: '/dataExcel/productDataLeadIn',
        method: 'get',
        data: data,
    });
};

/**
 * 简要商品信息导出
 * @param {*} data 
 * @returns 
 */
export const productDataLeadOut = data => {
    outFile(data)
};

/**
 * 积分订单导出
 * @param {*} data 
 * @returns 
 */
export const orderDataLeadOut = data => {
    outFile(data)
};
const outFile = (data) => {
    request({
        url: data.url,
        responseType: 'blob', // 表明返回服务器返回的数据类型
        method: 'post',
        headers: {
            'Content-type': 'application/json',
            'contentType': 'application/x-www-form-urlencoded',
            'Authorization': localStorage.getItem('Authorization')
        },
        data: data.ids
    }).then(result => {
        //创建一个Blob对象接收后端传来的文件流
        const blob = new Blob([result], {
            type: 'application/octet-stream'
        })
        const downloadElement = document.createElement('a')
        // 创建下载的链接
        const href = window.URL.createObjectURL(blob)
        downloadElement.href = href
        // 下载后文件名
        downloadElement.download = data.fileName
        document.body.appendChild(downloadElement)
        // 点击下载
        downloadElement.click()
        // 下载完成移除元素
        document.body.removeChild(downloadElement)
        // 释放掉blob对象
        window.URL.revokeObjectURL(href)
    }).catch(err => {
        ElMessage.error("数据发生异常，请联系管理员核查数据是否正确!\n" + err.message);
    })
}