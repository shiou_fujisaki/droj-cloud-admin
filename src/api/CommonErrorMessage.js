
const SUCCESS_DIALOG_MESSAGE = "操作成功";
const FAILD_DIALOG_MESSAGE = "操作失败";
const SUCCESS_ADDDATA_DIALOG_MESSAGE = "添加数据成功";
const SUCCESS_EDITDATA_DIALOG_MESSAGE = "修改数据成功";
const SUCCESS_DELDATA_DIALOG_MESSAGE = "删除数据成功";
const SUCCESS_BUNDDATA_DIALOG_MESSAGE = "绑定数据成功";
const NO_DATA_SELECT_ERROR = "请选择一条数据";
// -------------------------------------------------------------------------//
const ERROR_CHOOSE_DEL_MEMBER_EMPTY = "请选择一个要删除的用户";
const ERROR_CHOOSE_REC_MEMBER_EMPTY = "请选择一个要恢复的用户";
const ERROR_CHOOSE_ACT_MEMBER_EMPTY = "请选择一个要启用的用户";
const ERROR_CHOOSE_UNACT_MEMBER_EMPTY = "请选择一个要停用的用户";
const ERROR_CHOOSE_VIEW_MEMBER_EMPTY = "请选择一个用户";
// -------------------------------------------------------------------------//
const ERROR_CHOOSE_DEL_RESOUSE_EMPTY = "请选择一个要删除的菜单资源";
const ERROR_CHOOSE_REC_RESOUSE_EMPTY = "请选择一个要恢复的菜单资源";
const ERROR_CHOOSE_VIEW_RESOUSE_EMPTY = "请选择一个菜单资源";
// -------------------------------------------------------------------------//
const ERROR_CHOOSE_REC_ROLE_EMPTY = "请选择一个操作角色";
// -------------------------------------------------------------------------//
const ERROR_CHOOSE_DEL_DICTIONARY_EMPTY = "请选择一个字典资源";
// -------------------------------------------------------------------------//

const multipleSelection = [];
// 主数据总页数
const dataTotal = 0;
// 操作列宽
const optColumnWidth = 150;
const formLabelWidth = '170px';
//主数据条件查询
const query = { keyword: "", pageIndex: 1, pageSize: 10,}
export {
	SUCCESS_DIALOG_MESSAGE,FAILD_DIALOG_MESSAGE,
	// -------------------------------------------------------------------------//
	ERROR_CHOOSE_DEL_MEMBER_EMPTY,ERROR_CHOOSE_REC_MEMBER_EMPTY,ERROR_CHOOSE_ACT_MEMBER_EMPTY,ERROR_CHOOSE_UNACT_MEMBER_EMPTY,ERROR_CHOOSE_VIEW_MEMBER_EMPTY,
	// -------------------------------------------------------------------------//
	ERROR_CHOOSE_DEL_RESOUSE_EMPTY,ERROR_CHOOSE_REC_RESOUSE_EMPTY,ERROR_CHOOSE_VIEW_RESOUSE_EMPTY,
	// -------------------------------------------------------------------------//
	ERROR_CHOOSE_REC_ROLE_EMPTY,
	// -------------------------------------------------------------------------//
	ERROR_CHOOSE_DEL_DICTIONARY_EMPTY,
	// -------------------------------------------------------------------------//
	SUCCESS_ADDDATA_DIALOG_MESSAGE, SUCCESS_EDITDATA_DIALOG_MESSAGE, SUCCESS_DELDATA_DIALOG_MESSAGE, NO_DATA_SELECT_ERROR, SUCCESS_BUNDDATA_DIALOG_MESSAGE,
	multipleSelection, dataTotal, optColumnWidth, formLabelWidth, query, 
};