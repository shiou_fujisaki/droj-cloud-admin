import request from '../utils/request';
import AjaxRequest from '../utils/AjaxRequest';
/**
 * UC 会员中心API资源文件
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
export const memberlogin = data => {
    return AjaxRequest.request({
        url: '/login',
        method: 'post',
        data: data
    });
}
export const renterlogin = data => {
    return AjaxRequest.request({
        url: '/login',
        method: 'post',
        data: data
    });
}
export const check_auth = () => {
    return AjaxRequest.request({
        url: '/beat',
        method: 'get'
    });
}
// 菜单，资源，鉴权
export const auth = () => {
    return AjaxRequest.request({
        url: '/auth',
        method: 'get'
    });
}
export const sites = param => {
    return AjaxRequest.request({
        url: '/sys/sites',
        method: 'get'
    });
}