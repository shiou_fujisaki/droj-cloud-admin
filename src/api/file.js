import request from '../utils/request';
import AjaxRequest from '../utils/AjaxRequest';
//  --------------------------------------------------------------------------------------------------------------- //
// 视频文件上传
export const findFileListPage = data => {
    return AjaxRequest.request({
        url: '/file/findFileListPage',
        method: 'get',
        params: data,
    });
};
// 视频文件上传
export const findFileInfo = data => {
    return AjaxRequest.request({
        url: '/file/info/'+ data,
        method: 'get',
    });
};
// 视频文件上传
export const uploadVideo = data => {
    return AjaxRequest.request({
        url: '/file/uploadVideo',
        method: 'post',
		data: data,
    });
};
// 图片文件上传
export const uploadImage = data => {
    return AjaxRequest.request({
        url: '/file/uploadImage',
        method: 'post',
		data: data,
    });
};
// 文件上传
export const uploadFile = data => {
    return AjaxRequest.request({
        url: '/file/uploadFile',
        method: 'post',
		data: data,
    });
};
// 文件删除
export const delFile = data => {
    return AjaxRequest.request({
        url: '/file/del/'+data,
        method: 'delete',
    });
};