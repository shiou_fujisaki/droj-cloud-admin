import request from '../utils/request';
/**
 * UC 会员中心API资源文件
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
 var cust_base_uri_req = 'http://127.0.0.1:4000/ocrpic';
 export const BRAND_STRING = ["西壬", "晶粮康源", "九三", "海春园", "津良", "味春晓", "康倍多", "海皇", "塞外花", "长寿花", "长寿", "鸿鹤", "鲁姬香", "塞花香",
                "天益","金鼎", "福临门", "得乐康", "欧丽薇兰", "汇福", "五湖", "多力", "五丰", "金龙鱼", "鲁姬花", "姬花香", "冀满园", "胡姬花", "鲁花", "深安", "西王",
                "伊心爱", "元宝", "孚百香", "福之泉", "沃棉田", "馥龙", "孚大厨", "金御燕", "唐大妈", "聚美味", "宇师傅", "农家", "福掌柜", "菜子王", "鲤鱼",
                "石库门", "康源", "逸飞", "皖丰园", "恒大", "福满盈", "瑞禾", "君聚宝", "香满园", "道道全"]
// Customer 商品OCR管理 --------------------------------------------------------------------Start
/**
 * 选择OCR图片目录
 * @param {*} query 
 * @returns 
 */
 export const chooseOcrPicPath = query => {
    return request({
        url: cust_base_uri_req+'/choose',
        method: 'get',
        params: query
    });
};

/**
 * 选择识别OCR图片文件
 * @param {*} query 
 * @returns 
 */
export const chooseOcrVerifyPicPath = query => {
    return request({
        url: cust_base_uri_req+'/chooseVer',
        method: 'get',
        params: query
    });
};

/**
 * 平台商品OCR图片切割
 * @param {*} v 
 * @returns 
 */
 export const cutOcrPicData = v => {
    return request({
        url: cust_base_uri_req+'/cut',
        method: 'post',
        data: v
    });
};

/**
 * 平台商品OCR图片识别
 * @param {*} v 
 * @returns 
 */
 export const verifyPicData = v => {
    return request({
        url: cust_base_uri_req+'/ocr',
        method: 'post',
        data: v
    });
};

/**
 * 平台商品OCR图片识别
 * @param {*} v 
 * @returns 
 */
 export const cleanVerifyData = v => {
    return request({
        url: cust_base_uri_req+'/cla',
        method: 'post',
        data: v
    });
};