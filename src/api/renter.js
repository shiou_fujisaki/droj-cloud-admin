import request from '../utils/request';
import AjaxRequest from '../utils/AjaxRequest';
import { ElMessage, ElMessageBox } from 'element-plus';
/**
 * UC 租户中心API资源文件
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
export const Renterlogin = data => {
    return AjaxRequest.request({
        url: '/login',
        method: 'post',
        data: data
    });
}
export const renterlogin = data => {
    return AjaxRequest.request({
        url: '/login',
        method: 'post',
        data: data
    });
}
export const check_auth = () => {
    return AjaxRequest.request({
        url: '/beat',
        method: 'get'
    });
}
export const sites = param => {
    return AjaxRequest.request({
        url: '/sys/sites',
        method: 'get'
    });
}

// Renter 租户管理 --------------------------------------------------------------------Start
/**
 * 平台租户查询
 * @param {*} query 
 * @returns 
 */
export const fetchRenterData = query => {
    let _obj =  AjaxRequest.request({
        url: '/uc/fetchRenterlist',
        method: 'get',
        params: query
    });
    _obj.then((res) => {
        if(res.code != 200){
            if (res.type == 0) {
                ElMessage.warning(res.message);
            } else {
                ElMessage.error(res.message);
            }
        }
    });
    return _obj;
};
/**
 * 平台租户详细查询
 * @param {*} query 
 * @returns 
 */
 export const fetchRenterInfoData = query => {
    return AjaxRequest.request({
        url: '/uc/info/'+query,
        method: 'get',
    });
};
export const addRenter = v => {
    return AjaxRequest.request({
        url: '/uc/saveRenterInfo',
        method: 'post',
        data: v
    });
};
export const saveRenter = v => {
    return AjaxRequest.request({
        url: '/uc/saveRenterInfo',
        method: 'put',
        data: v
    });
};
/**
 * 平台租户批量删除
 * @param {*} query 
 * @returns 
 */
 export const batchDelRenter = v => {
    return AjaxRequest.request({
        url: '/uc/batchDelete',
        method: 'delete',
        data: v
    });
};

/**
 * 平台租户批量更新状态
 * @param {*} query 
 * @returns 
 */
 export const batchActRenter = v => {
    return AjaxRequest.request({
        url: '/uc/batchUpdateStatus',
        method: 'put',
        data: v
    });
};
// Renter 租户管理 --------------------------------------------------------------------End
// Renter 角色管理 --------------------------------------------------------------------Start
/**
 * 平台角色查询
 * @param {*} query 
 * @returns 
 */
export const fetchRoleListData = query => {
    return AjaxRequest.request({
        url: '/uc/role/fetchRolelist',
        method: 'get',
        params: query
    });
};
export const fetchUseRolelist = query => {
    return AjaxRequest.request({
        url: '/uc/role/fetchUseRolelist',
        method: 'get',
        params: query
    });
};
/**
 * 平台角色查询
 * @param {*} query 
 * @returns 
 */
 export const findRoleInfoById = query => {
    return AjaxRequest.request({
        url: '/uc/role/info/'+query,
        method: 'get',
    });
};

/**
 * 平台角色下拉选项查询
 * @param {*} query 
 * @returns 
 */
 export const fetchRoleOptData = () => {
    return AjaxRequest.request({
        url: '/uc/role/fetchRoleOptData',
        method: 'get'
    });
};
/**
 * 系统角色编辑保存
 * @param {*} query 
 * @returns 
 */
 export const addRole = v => {
    return AjaxRequest.request({
        url: '/uc/role/addRole',
        method: 'post',
        data: v
    });
};
/**
 * 系统角色编辑保存
 * @param {*} query 
 * @returns 
 */
 export const saveRole = v => {
    return AjaxRequest.request({
        url: '/uc/role/saveRole',
        method: 'put',
        data: v
    });
};
/**
 * 批量删除系统角色
 * @param {*} query 
 * @returns 
 */
 export const batchDelRoler = v => {
    return AjaxRequest.request({
        url: '/uc/role/delete',
        method: 'delete',
        data: v
    });
};
/**
 * 批量激活关闭/恢复系统角色状态
 * @param {*} query 
 * @returns 
 */
 export const batchActRoler = v => {
    return AjaxRequest.request({
        url: '/uc/role/updateActiveRoleStatus',
        method: 'put',
        data: v
    });
};
// Renter 角色管理 --------------------------------------------------------------------End
// Renter 资源管理 --------------------------------------------------------------------Start
/**
 * 系统可访问资源查询
 * @param {*} query 
 * @returns 
 */
export const fetchResouseData = query => {
    return AjaxRequest.request({
        url: '/uc/resource/fetchResourcelist',
        method: 'get',
        params: query
    });
};
/**
 * 系统可访问资源查询
 * @param {*} query 
 * @returns 
 */
 export const fetchOptionResouseData = query => {
    return AjaxRequest.request({
        url: '/uc/resource/fetchOptions',
        method: 'get',
        params: query
    });
};
/**
 * 系统可访问资源查询
 * @param {*} query 
 * @returns 
 */
 export const fetchResouseInfoData = query => {
    return AjaxRequest.request({
        url: '/uc/resource/info'+query,
        method: 'get'
    });
};

/**
 * 系统资源编辑保存
 * @param {*} query 
 * @returns 
 */
 export const addResouser = v => {
    return AjaxRequest.request({
        url: '/uc/resource/update',
        method: 'post',
        data: v
    });
};
/**
 * 系统资源编辑保存
 * @param {*} query 
 * @returns 
 */
 export const saveResouser = v => {
    return AjaxRequest.request({
        url: '/uc/resource/update',
        method: 'put',
        data: v
    });
};
/**
 * 系统资源批量删除
 * @param {*} query 
 * @returns 
 */
 export const batchDelResouser = v => {
    return AjaxRequest.request({
        url: '/uc/resource/delete',
        method: 'delete',
        data: v
    });
};
/**
* 系统资源批量回收
* @param {*} query 
* @returns 
*/
export const batchRecResouser = v => {
   return true;
};
/**
 * 系统资源状态变更
 * @param {*} query 
 * @returns 
 * @description 普通租户不可见资源 / 普通租户可见资源
 */
 export const batchUpdateStatus = v => {
    return AjaxRequest.request({
        url: '/uc/resource/updateStatus',
        method: 'put',
        data: v
    });
};
// Renter 资源管理 --------------------------------------------------------------------End
// Renter 权限管理 --------------------------------------------------------------------Start

/**
 * 权限查询
 * @param {*} query 
 * @returns 
 */
 export const fetchPermissionListData = query => {
    return AjaxRequest.request({
        url: '/uc/permission/fetchPermissionlist',
        method: 'get',
        params: query
    });
};
/**
 * 操作权限状态
 * @param {*} query 
 * @returns 
 */
 export const batchEditPermissionStatus = data => {
    return AjaxRequest.request({
        url: '/uc/permission/updateStatus',
        method: 'put',
        data: data
    });
};
/**
 * 操作权限状态
 * @param {*} query 
 * @returns 
 */
 export const deletePermissionData = query => {
    return AjaxRequest.request({
        url: '/uc/permission/delete/'+query,
        method: 'delete',
    });
};
// Renter 权限管理 --------------------------------------------------------------------End
// Renter 租户角色管理 --------------- ------------------------------------------------Start
/**
 * 角色内租户关联查询
 * @param {*} query 
 * @returns 
 */
export const fetchRoleRenters = query =>{
    return AjaxRequest.request({
        url: '/uc/role/members/roleId',
        method: 'get',
        params: query
    });
};
/**
 * 租户内角色关联查询
 * @param {*} query 
 * @returns 
 */
 export const fetchRenterRoles = query => {
    return AjaxRequest.request({
        url: '/uc/role/roles/memberId',
        method: 'get',
        params: query
    });
};
/**
 * 角色内租户关联编辑
 * @param {*} query 
 * @returns 
 */
export const editRoleRenterConn = v => {
    return AjaxRequest.request({
        url: '/uc/role/grant',
        method: 'put',
        data: v
    });
};
/**
 * 角色内租户关联删除
 * @param {*} query 
 * @returns 
 */
export const delRoleRenterConn = v => {
    return AjaxRequest.request({
        url: '/uc/role/delRenterConn',
        method: 'delete',
        data: v
    });
};
// Renter 租户角色管理 --------------- ------------------------------------------------end
// Renter 资源角色管理 --------------- ------------------------------------------------start
/**
 * 角色资源查询
 * @param {*} query 
 * @returns 
 */
 export const fetchRoleResouses = query =>{
    return AjaxRequest.request({
        url: '/uc/role/resouces',
        method: 'get',
        params: query
    });
};
/**
 * 获取系统资源的目录树
 * @param {*} query 
 * @returns 
 */
 export const fetchResouseSubTreeList = query => {
    return AjaxRequest.request({
        url: '/uc/role/resouces/tree',
        method: 'get',
        params: query
    });
}
/**
 * 绑定角色资源
 * @param {*} query 
 * @returns 
 */
export const editRoleResouseConn = v =>{
    return AjaxRequest.request({
        url: '/uc/role/bundRoleResources',
        method: 'post',
        data: v
    });
};

/**
 * 识别角色访问资源父子关系
 * @param {*} query 
 * @returns 
 */
 export const checkedResConn = v =>{
    return AjaxRequest.request({
        url: '/uc/role/checked',
        method: 'put',
        data: v
    });
};
// Renter 资源角色管理 --------------- ------------------------------------------------end
// Renter 权限角色管理 --------------- ------------------------------------------------start
/**
 * 角色权限查询
 * @param {*} query 
 * @returns 
 */
 export const fetchRolePermissions = query => {
    return AjaxRequest.request({
        url: '/uc/role/permissions',
        method: 'get',
        params: query
    });
};
/**
 * 获取系统资源的目录树
 * @param {*} query 
 * @returns 
 */
 export const fetchPermissionSubTreeList = query => {
    return AjaxRequest.request({
        url: '/uc/role/permissions/tree',
        method: 'get',
        params: query
    });
}
/**
 * 绑定角色权限
 * @param {*} query 
 * @returns 
 */
export const editRolePermissionConn = v =>{
    return AjaxRequest.request({
        url: '/uc/role/bundRolePermission',
        method: 'post',
        data: v
    });
};
// Renter 权限角色管理 --------------- ------------------------------------------------end