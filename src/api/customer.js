import request from '../utils/request';
/**
 * UC 会员中心API资源文件
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
 var cust_base_uri_req = 'http://127.0.0.1:6300/custa';
// Customer 商户管理 --------------------------------------------------------------------Start
/**
 * 平台商户查询
 * @param {*} query 
 * @returns 
 */
 export const fetchCustomerData = query => {
    return request({
        url: cust_base_uri_req+'/c/fetch',
        method: 'get',
        params: query
    });
};