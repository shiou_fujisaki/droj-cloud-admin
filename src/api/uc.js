import request from '../utils/request';
import AjaxRequest from '../utils/AjaxRequest';
/**
 * UC 会员中心API资源文件
 * 
 * @author 小兔
 * @Date 20210905
 * @description
 */
export const sites = query => {
    return AjaxRequest.request({
        url: '/sys/sites',
        method: 'get',
        params: query
    });
 }
// Member 会员管理 --------------------------------------------------------------------Start
/**
 * 平台用户查询
 * @param {*} query 
 * @returns 
 */
export const fetchMemberData = query => {
    return AjaxRequest.request({
        url: '/member/findSysMemberListPage',
        method: 'get',
        params: query
    });
};
/**
 * 平台用户详细查询
 * @param {*} query 
 * @returns 
 */
 export const fetchMemberInfoData = query => {
    return AjaxRequest.request({
        url: '/member/info/'+query,
        method: 'get',
    });
};
export const addSysMember = v => {
    return AjaxRequest.request({
        url: '/member/saveSysMemberInfo',
        method: 'post',
        data: v
    });
};
export const saveSysMember = v => {
    return AjaxRequest.request({
        url: '/member/saveSysMemberInfo',
        method: 'put',
        data: v
    });
};
/**
 * 平台用户批量删除
 * @param {*} query 
 * @returns 
 */
 export const batchDelMember = v => {
    return AjaxRequest.request({
        url: '/member/deleteSysMember',
        method: 'post',
        data: v
    });
};
/**
 * 平台用户批量回收
 * @param {*} query 
 * @returns 
 */
 export const batchRecMember = v => {
    return AjaxRequest.request({
        url: '/member/batchEditSysMemberStatus',
        method: 'put',
        data: v
    });
};
/**
 * 平台用户批量回收
 * @param {*} query 
 * @returns 
 */
 export const batchActMember = v => {
    return AjaxRequest.request({
        url: '/member/batchEditSysMemberStatus',
        method: 'put',
        data: v
    });
};
// Member 会员管理 --------------------------------------------------------------------End
// Member 角色管理 --------------------------------------------------------------------Start
/**
 * 平台角色查询
 * @param {*} query 
 * @returns 
 */
export const fetchRoleListData = query => {
    return AjaxRequest.request({
        url: '/member/role/fetchRolelist',
        method: 'get',
        params: query
    });
};
/**
 * 平台角色下拉选项查询
 * @param {*} query 
 * @returns 
 */
 export const fetchRoleOptData = query => {
    return AjaxRequest.request({
        url: '/member/role/fetchRoleOptData',
        method: 'get',
        params: query
    });
};
/**
 * 系统角色编辑保存
 * @param {*} query 
 * @returns 
 */
 export const editRole = v => {
    return AjaxRequest.request({
        url: '/r/editRoler',
        method: 'post',
        data: v
    });
};
/**
 * 批量删除系统角色
 * @param {*} query 
 * @returns 
 */
 export const batchDelRoler = v => {
    return AjaxRequest.request({
        url: '/r/batchDelRoler',
        method: 'post',
        data: v
    });
};
/**
 * 批量恢复系统角色
 * @param {*} query 
 * @returns 
 */
export const batchRecRoler = v => {
    return AjaxRequest.request({
        url: '/r/batchRecRoler',
        method: 'post',
        data: v
    });
};
/**
 * 批量激活关闭角色状态
 * @param {*} query 
 * @returns 
 */
 export const batchActRoler = v => {
    return AjaxRequest.request({
        url: '/r/batchActRoler',
        method: 'post',
        data: v
    });
};
// Member 角色管理 --------------------------------------------------------------------End
// Member 资源管理 --------------------------------------------------------------------Start
/**
 * 系统可访问资源查询
 * @param {*} query 
 * @returns 
 */
export const fetchResouseData = query => {
    return AjaxRequest.request({
        url: '/re/fetch',
        method: 'get',
        params: query
    });
};
/**
 * 系统可访问资源查询
 * @param {*} query 
 * @returns 
 */
 export const fetchOptionResouseData = query => {
    return AjaxRequest.request({
        url: '/re/fetchOptions',
        method: 'get',
        params: query
    });
};
/**
 * 系统资源编辑保存
 * @param {*} query 
 * @returns 
 */
 export const editResouser = v => {
    return AjaxRequest.request({
        url: '/re/editResouser',
        method: 'post',
        data: v
    });
};
/**
 * 系统资源批量删除
 * @param {*} query 
 * @returns 
 */
 export const batchDelResouser = v => {
    return AjaxRequest.request({
        url: '/re/batchDelResouser',
        method: 'post',
        data: v
    });
};
/**
 * 系统资源批量回收
 * @param {*} query 
 * @returns 
 */
 export const batchRecResouser = v => {
    return AjaxRequest.request({
        url: '/re/batchRecResouser',
        method: 'post',
        data: v
    });
};
/**
 * 系统资源批量回收
 * @param {*} query 
 * @returns 
 * @description 普通用户不可见资源
 */
 export const batchHiddenResouser = v => {
    return AjaxRequest.request({
        url: '/re/batchHiddenResouser',
        method: 'post',
        data: v
    });
};
/**
 * 系统资源批量回收
 * @param {*} query 
 * @returns 
 * @description 普通用户可见资源
 */
 export const batchUnHiddenResouser = v => {
    return AjaxRequest.request({
        url: '/re/batchUnHiddenResouser',
        method: 'post',
        data: v
    });
};
// Member 资源管理 --------------------------------------------------------------------End
// Member 权限管理 --------------------------------------------------------------------Start
/**
 * 权限查询
 * @param {*} query 
 * @returns 
 */
export const fetchRolePermissions = query => {
    return AjaxRequest.request({
        url: '/member/role/permissions',
        method: 'get',
        params: query
    });
};
// Member 权限管理 --------------------------------------------------------------------End
// Member 用户角色管理 --------------- ------------------------------------------------Start
/**
 * 角色内用户关联查询
 * @param {*} query 
 * @returns 
 */
export const fetchRoleMembers = query =>{
    return AjaxRequest.request({
        url: '/member/role/members/roleId',
        method: 'get',
        params: query
    });
};
/**
 * 用户内角色关联查询
 * @param {*} query 
 * @returns 
 */
 export const fetchMemberRoles = query => {
    return AjaxRequest.request({
        url: '/member/role/roles/memberId',
        method: 'get',
        params: query
    });
};
/**
 * 角色内用户关联编辑
 * @param {*} query 
 * @returns 
 */
export const editRoleMemberConn = v => {
    return AjaxRequest.request({
        url: '/r/edit/roleMemberConn',
        method: 'post',
        data: v
    });
};
/**
 * 角色内用户关联删除
 * @param {*} query 
 * @returns 
 */
export const delRoleMemberConn = v => {
    return AjaxRequest.request({
        url: '/r/edit/delMemberConn',
        method: 'post',
        data: v
    });
};
// Member 用户角色管理 --------------- ------------------------------------------------end
// Member 资源角色管理 --------------- ------------------------------------------------start
export const fetchRoleResouses = query =>{
    return AjaxRequest.request({
        url: '/member/role/resouces',
        method: 'get',
        params: query
    });
};
export const editRoleResouseConn = v =>{
    return AjaxRequest.request({
        url: '/r/edit/roleResouseConn',
        method: 'post',
        data: v
    });
};
export const delRoleResouseConn = v =>{
    return AjaxRequest.request({
        url: '/r/edit/delResouseConn',
        method: 'post',
        data: v
    });
};
// Member 资源角色管理 --------------- ------------------------------------------------end