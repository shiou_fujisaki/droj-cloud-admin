export function formatDate(date,fmt) {
    // if(/(y+)/.test(fmt)){
    //     fmt = fmt.replace(RegExp.$1,(date.getFullYear() + '').substr(4 - RegExp.$1.length))
    // }
    // let o ={
    //     'M+': date.getMonth()+1,
    //     'd+': date.getDate(),
    //     'h+': date.getHours(),
    //     'm+': date.getMinutes(),
    //     's+': date.getSeconds()
    // };
    // for (let k in o){
    //     if(new RegExp(`(${k})`).test(fmt))
    //     {
    //         let str = o[k] + '';
    //         fmt=fmt.replace(RegExp.$1,(RegExp.$1.length===1)?str:padLeftZero(str))
    //     }
 
    // }
    // return fmt
	//当前日期格式化
	var date = new Date()
	var year = date.getFullYear()
	var month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth()+ 1
	var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
	// var hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours()
	// var minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()
	// var seconds = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds()
	return year + '-' +month + '-' + day
}
function padLeftZero(str){
    return ('00' + str).substr(str.length)
}