import axios from "axios"   //封装一个比较好用的 ajax 
import store from "../store"
// import { router } from '@/router/index'
// import { getLocal } from "../libs/local"

class AjaxRequest {
    constructor() {
        let huoshangyun = "175.24.116.12";
        this.baseURL = localStorage.getItem("baseURL");
        this.timeout = 5000;//访问接口时间 超时了  3s 
        this.queue = {}; // 存放每一次的请求
        this.headers = {
            'Content-type': 'application/json',
            'contentType': 'application/x-www-form-urlencoded',
            'Authorization': localStorage.getItem('Authorization')
        }
    }
    merge (options) {
        return { ...options, baseURL: this.baseURL, timeout: this.timeout }
    }
    setInterceptor (instance, url) {
        // 每次请求时，都要加上一个loading效果
        instance.interceptors.request.use((config) => {
            var Authorization = localStorage.getItem("Authorization");
            var sid = localStorage.getItem("siteSno");
            var authCode = localStorage.getItem("authCode");
            // 在请求拦截中，每次请求，都会加上一个Authorization头
            config.headers = { Authorization: Authorization, "sid": sid ,"authCode":authCode};
            // 第1次请求时，显示Loading动画
            // if (Object.keys(this.queue).length === 0) {
            //     store.commit("showLoading", true); //只让小动画 显示一次  后来在一直请求 就不用一直转转
            // }
            this.queue[url] = url;
            return config;
        }, err => {
            console.log(err);
        });
        instance.interceptors.response.use((res) => {
            delete this.queue[url];
            if (res.data['code'] === 401) {
                // debugger; SUOTB DEBUG 请求关闭401权限登录错误
                window.sessionStorage.clear();
                window.localStorage.clear();
                let _login_path = localStorage.getItem("baseURL")+"/login";
                router.replace({ path: _login_path });
                return res;
            }
            return res.data;  //相应拦截 过滤数据 
        }, err => {
            console.log(err);
        });
    }
    request (options) {
        let _url = '';
        if(options.url === '/login' || options.url === '/beat'|| options.url === '/auth'){
            _url = 'http://www.ihuoyu.club/sso' + options.url;
        } else {
            _url = localStorage.getItem("baseURL") + options.url;
        }
        let instance = axios.create(); //创建一个ajax实例 发出请求  
        this.setInterceptor(instance, _url); // 设置拦截
        let config = this.merge(options);
        if(options.url === '/login' || options.url === '/beat' || options.url === '/auth' || options.url === '/sys/sites'){
            config['baseURL'] = 'http://www.ihuoyu.club/sso';
        }
        return instance(config)  //实例 表示调用ajax   返回一个ajax的实例 
    }
}

export default new AjaxRequest;