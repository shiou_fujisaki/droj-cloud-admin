/*
* @Authoer: weimei
* @Description: 菜单配置
* @Date 2017/12/10 9：30
* @Modified By:
*/
define(function (require, exports) {
    exports.ready = function (hashCode, data, cite) {
        var pathId = data.pjtData.pkId;
        var nm = data.pjtData.nodeId;
        var bus = data.bus;
        var msgId = data.basicData.respMsgId;
        var vm = yufp.custom.vue({
            el: "#tradeRespMsg",
            data: function () {
                var me = this;
                return {
                    upLoadData: {
                        access_token: yufp.service.getToken()
                    },

                    gridData: [],
                    gridDataTmp: [],
                    reqId: "",
                    busTel: {},
                    baseParam: {
                        trdInfId: pathId
                    },

                    /*-------------------↓↓↓↓↓↓----------------------数据集相关的数据------------↓↓↓↓↓-------------------*/
                    addOrUpd: 'add',
                    activeName: 'data1',
                    reqMsgDialogVisible: false,
                    reqMsgFormDisabled: false,
                    viewTitle: yufp.lookup.find('CRUD_TYPE', false),
                    viewType: '',
                    reqMsgQueryFields: [
                        {placeholder: '模糊搜索', field: 'name', type: 'input'}
                    ],

                    reqMsgQueryButtons: [
                        {
                            label: '查询',
                            op: 'submit',
                            type: 'primary',
                            icon: "search",
                            click: function (model, valid) {
                                if (valid) {
                                    var param = {
                                        condition: JSON.stringify({
                                            caseNm: model.name ? model.name : null,
                                        })
                                    };
                                    me.$refs.reqMsgTable.remoteData(param);
                                }
                            }
                        }
                    ],
                    reqMsgUrl: '/api/ymit/dataset/',
                    reqMsgChgUrl: backend.adminService + '/api/chgLog/',
                    reqMsgAddUrl: '/api/ymit/dataset/add',
                    reqMsgUpdateUrl: '/api/ymit/dataset/updateset',
                    reqMsgDelUrl: '/api/ymit/dataset/setdelete/',
                    reqMsgFldUrl: backend.adminService + '/api/bspmsg/getmsg',
                    saveTelUrl: backend.adminService + '/api/biztmp/add',

                    reqMsgColumns: [
                        {
                            label: '数据及名称',
                            prop: 'datasetNm',
                            sortable: true,
                            resizable: true,
                            showOverflowTooltip: true,
                            template: function () {
                                return '<template scope="scope">\
                                <a style="text-decoration:underline;" @click="_$event(\'custom-row-click\', scope)">{{ scope.row.datasetNm }}</a>\
                            </template>';
                            }
                        },
                        {
                            label: '数据集类型',
                            prop: 'datasetTyp',
                            sortable: true,
                            resizable: true,
                            showOverflowTooltip: true
                        },
                        {
                            label: '引用次数',
                            prop: 'referNm',
                            sortable: true,
                            resizable: true,
                            showOverflowTooltip: true
                        },
                        {label: '包含数据条数', prop: 'itemNm', sortable: true, resizable: true,},
                        {label: '引用实例', prop: 'referCase', sortable: true, resizable: true,},
                        {label: '数据集描述', prop: 'datasetDesc', sortable: true, resizable: true},
                        {label: '创建者', prop: 'crtUsr', sortable: true, resizable: true},
                        {label: '创建时间', prop: 'crtDt', sortable: true, resizable: true},
                        {label: '交易接口id', prop: 'trdInfId', sortable: true, resizable: true, hidden: true},
                        {label: '主键', prop: 'pkId', sortable: true, resizable: true, hidden: true},
                    ],
                    reqMsgChgColumns: [
                        {label: '变更时间', prop: 'chgDesc', sortable: true, resizable: true,},
                        {label: '变更者', prop: 'chgUsrId', sortable: true, resizable: true,},
                        {label: '变更描述', prop: 'datasetDesc', sortable: true, resizable: true}
                    ],
                    reqMsgFields: [
                        {
                            columnCount: 1,
                            fields: [
                                {
                                    field: 'datasetNm', label: '数据集名称 ', disabled: false, type: 'input', rules: [
                                        {required: true, message: '必填项', trigger: 'blur'}
                                    ]
                                },
                                {
                                    field: 'datasetTyp',
                                    label: '数据集类型',
                                    disabled: false,
                                    type: 'select',
                                    dataCode: 'DATA_STS',
                                    rules: [
                                        {required: true, message: '必填项', trigger: 'blur'}
                                    ]
                                },
                                {field: 'datasetDesc', label: '数据集描述', disabled: false, type: 'textarea', rows: 3},
                                {
                                    field: 'trdInfId',
                                    label: '交易接口id ',
                                    disabled: false,
                                    hidden: true,
                                    type: 'input',
                                    rules: [
                                        {required: false, message: '必填项', trigger: 'blur'}
                                    ]
                                },
                                {
                                    field: 'pkId', label: '主键 ', disabled: false, type: 'input', hidden: true, rules: [
                                        {required: false, message: '必填项', trigger: 'blur'}
                                    ]
                                },

                            ]
                        }
                    ],
                    busTelFields: [
                        {
                            columnCount: 3,
                            fields: [
                                {
                                    field: 'trdInfNm', label: '交易接口名称 ', disabled: false, type: 'input'
                                },
                                {
                                    field: 'crtUsrId', label: '创建者', disabled: false, type: 'input'
                                },
                                {
                                    field: 'crtTm', label: '创建时间', disabled: false, type: 'input'
                                },

                            ]
                        }
                    ],
                    reqMsgFormButtons: [
                        {
                            label: '取消', type: 'primary', icon: "yx-undo2", hidden: false, click: function (model) {
                                me.reqMsgDialogVisible = false;
                            }
                        },
                        {
                            label: '下一步',
                            type: 'primary',
                            icon: "check",
                            hidden: false,
                            op: 'submit',
                            click: function (model) {
                                //me.saveReqMsg();
                                me.activeName = "data2";
                                //加载函数
                                me.loadFldInf();
                            }
                        },
                        {
                            label: '下一步',
                            type: 'primary',
                            icon: "check",
                            hidden: false,
                            op: 'submit',
                            click: function (model) {
                                me.activeName = "data2";
                                //me.updateReaMsg();
                                //加载初始化函数
                                me.initFldInf();
                            }
                        }
                    ],

                    multipleSelection: [],
                    /*-------------------↑↑↑↑↑↑↑↑↑----------------------测试用例相关的数据---------↑↑↑↑↑↑↑↑↑----------------------*/
                    /*-------------------↓↓↓↓↓↓----------------------数据集新增的操作列------------↓↓↓↓↓-------------------*/
                    cols: [
                        /* {label: '修改值0', resizable: 'true', prop: 'fixVal',hidden:true},*/
                        {label: '数据1', resizable: 'true', prop: 'defVal1'},

                    ],
                    colNm: 1,
                    reqMsgSetForm: {
                        data: []
                    },
                    /*-------------------↑↑↑↑↑↑↑↑↑----------------------数据集新增的操作列---------↑↑↑↑↑↑↑↑↑----------------------*/
                }
            },
            methods: {
                /*数据集字段信息的加载*/
                loadFldInf: function () {
                    if ("全量字段" == "全量段") {
                        this.allDataLoad();
                    } else {
                        this.temFldLoad();
                    }
                },
                /* 数据集字段修改数据值时加载原数据*/
                initFldInf: function () {
                    if ("quanl") {
                    } else {
                    }
                },
                loadChgLog: function () {
                    var param = {
                        condition: JSON.stringify({
                            trdInfId: pathId
                        })
                    };
                    this.$refs.reqMsgChgTable.remoteData(param);
                },
                addCol() {
                    this.colNm++;
                    this.cols.push({label: '数据' + this.colNm, resizable: 'true', prop: 'defVal' + this.colNm})
                },
                next() {
                    if (this.active++ > 2) this.active = 0;
                },
                /*-------------------↓↓↓↓↓↓----------------------数据集相关的函数------------↓↓↓↓↓-------------------*/
                //点击新增按钮后的响应事件
                reqMsgAddFn: function () {
                    this.reqMsgDialogVisible = true;

                    this.$nextTick(function () {
                        this.$refs.reqMsgForm.resetFields();
                        this.viewType = 'ADD';
                        this.addOrUpd = 'ADD';
                        this.$refs.reqMsgForm.trdInfId = pathId;
                        this.reqMsgFormButtons[2].hidden = true;
                        this.reqMsgFormButtons[1].hidden = false;
                    });

                },
                /* 拼接数据*/
                valuesCannect: function (datas) {
                    var grp = [];
                    for (var col of this.cols) {
                        var list = [];
                        for (var row of datas) {
                            var iterm = {
                                fldNm: null,
                                fldDesc: null,
                                defVal: null,
                                datagrpId: col.prop,
                                datagrpNm: col.prop,
                                fldVal: null
                            };
                            iterm.fldNm = row.fldNm;
                            iterm.fldDesc = row.fldDesc;
                            iterm.defVal = row.defVal;
                            iterm.fldVal = row[col.prop];
                            list.push(iterm);
                        }
                        grp.push(list);
                    }
                    return grp;
                },
                //数据集的保存操作
                saveReqMsg: function () {
                    var _this = this;
                    var dataaa = _this.reqMsgSetForm.data;
                    /*将数据进行拼接*/
                    var iter = _this.valuesCannect(dataaa);
                    console.log();
                    var commit = {
                        datasetNm: _this.$refs.reqMsgForm.formModel.datasetNm,
                        datasetTyp: _this.$refs.reqMsgForm.formModel.datasetTyp,
                        datasetDesc: _this.$refs.reqMsgForm.formModel.datasetDesc,
                        trdInfId: pathId,
                        pkId: '',
                        listFld: iter
                    };

                    _this.$refs.reqMsgForm.validate(function (valid) {
                        if (valid) {
                            _this.$refs.reqMsgForm.formModel.trdInfId = pathId;
                            yufp.service.request({
                                method: 'POST',
                                url: _this.reqMsgAddUrl,
                                data: commit,
                                callback: function (code, message, response) {
                                    _this.dialogVisible = false;
                                    // _this.cols=_this.cols[0];
                                    if (response.code == 0) {
                                        _this.$message({message: '数据保存成功！'});
                                    } else {
                                        _this.$message({message: '数据保存失败！'});
                                    }
                                    var tableParam = {
                                        condition: JSON.stringify({
                                            parConstId: _this.funcId ? _this.funcId : null
                                        })
                                    };
                                    _this.$refs.reqMsgTable.remoteData(tableParam);
                                    _this.reqMsgDialogVisible = false;
                                }
                            });
                        } else {
                            _this.$message({message: '请检查输入项是否合法', type: 'warning'});
                            return false;
                        }
                    });
                },

                /*点击超链接时触发*/
                detail: function () {

                    this.reqMsgDialogVisible = true;
                    this.$nextTick(function () {
                        this.viewType = 'EDIT';
                        this.addOrUpd = 'EDIT';
                        this.reqMsgFormButtons[2].hidden = false;
                        this.reqMsgFormButtons[1].hidden = true;
                        yufp.extend(this.$refs.reqMsgForm.formModel, this.$refs.reqMsgTable.selections[0]);
                        /*加载数据集的字段信息*/
                        this.initSetIns();
                    });
                },
                //数据集用例修改按钮
                reqMsgModifyFn: function () {
                    if (this.$refs.reqMsgTable.selections.length < 1) {
                        this.$message({message: '请先选择一条记录', type: 'warning'})
                        return;
                    }
                    if (this.$refs.reqMsgTable.selections.length > 1) {
                        this.$message({message: '请选择一条记录', type: 'warning'})
                        return;
                    }
                    this.reqMsgDialogVisible = true;
                    this.$nextTick(function () {
                        this.viewType = 'EDIT';
                        this.addOrUpd = 'EDIT';
                        this.reqMsgFormButtons[2].hidden = false;
                        this.reqMsgFormButtons[1].hidden = true;
                        yufp.extend(this.$refs.reqMsgForm.formModel, this.$refs.reqMsgTable.selections[0]);
                        /*加载数据集的字段信息*/
                        this.initSetIns();
                    });
                },
                initSetIns: function () {
                    var _this = this;
                    var trdInfId = this.$refs.reqMsgTable.selections[0].trdInfId;
                    var datasetTyp = this.$refs.reqMsgTable.selections[0].datasetTyp;
                    var data = this.$refs.reqMsgTable.selections[0];
                    yufp.service.request({
                        method: 'POST',
                        url: backend.adminService + '/api/ymit/dataset/getset',
                        data: data,
                        callback: function (code, message, response) {
                            var data = response.data;
                            var leng = data.listFld;
                            _this.dataToTable(leng);

                        }
                    });

                },
                /*返回数据处理*/
                dataToTable: function (data) {
                    var table = [];
                    table = data[0];
                    for (var i = 0; i < data.length - 1; i++) {
                        this.addCol();
                    }
                    for (var j = 0; j < data.length; j++) {
                        for (var k = 0; k < data[j].length; k++) {
                            var name = 'defVal' + (j + 1);
                            table[k][name] = data[j][k].fldVal;
                        }
                    }
                    this.reqMsgSetForm.data = table;
                },

                //修改数据集保存
                updateReaMsg: function () {
                    var _this = this;
                    var dataaa = _this.reqMsgSetForm.data;
                    /*将数据进行拼接*/
                    var iter = _this.valuesCannect(dataaa);
                    console.log();
                    var commit = {
                        datasetNm: _this.$refs.reqMsgForm.formModel.datasetNm,
                        datasetTyp: _this.$refs.reqMsgForm.formModel.datasetTyp,
                        datasetDesc: _this.$refs.reqMsgForm.formModel.datasetDesc,
                        trdInfId: _this.$refs.reqMsgForm.formModel.trdInfId,
                        pkId: _this.$refs.reqMsgForm.formModel.pkId,
                        listFld: iter
                    };

                    _this.$refs.reqMsgForm.validate(function (valid) {
                        if (valid) {
                            _this.$refs.reqMsgForm.formModel.trdInfId = pathId;
                            yufp.service.request({
                                method: 'POST',
                                url: _this.reqMsgUpdateUrl,
                                data: commit,
                                callback: function (code, message, response) {
                                    _this.dialogVisible = false;
                                    // _this.cols= _this.cols[0];
                                    if (response.code == 0) {
                                        _this.$message({message: '数据保存成功！'});
                                    } else {
                                        _this.$message({message: '数据保存失败！'});
                                    }
                                    var tableParam = {
                                        condition: JSON.stringify({
                                            parConstId: _this.funcId ? _this.funcId : null
                                        })
                                    };
                                    _this.$refs.reqMsgTable.remoteData(tableParam);
                                    _this.reqMsgDialogVisible = false;
                                }
                            });
                        } else {
                            _this.$message({message: '请检查输入项是否合法', type: 'warning'});
                            return false;
                        }
                    });
                },

                /* 点击弹出框的保存按钮*/
                saveAndUpd: function () {
                    if (this.addOrUpd == 'ADD') {
                        this.saveReqMsg();
                    } else {
                        this.updateReaMsg();
                    }

                },
                //全量字段到模板的保存
                saveTel: function () {
                    var _this = this;
                    var commit = {};
                    var comitData = {
                        bizTmpNm: null,
                        trdInfId: null,
                        trdInfNm: nm,
                        msgId: msgId,
                        ymitMsgFld: []
                    };


                    comitData.bizTmpNm = 'kong';
                    comitData.trdInfId = pathId;
                    comitData.trdInfNm = nm;
                    comitData.msgId = msgId;

                    for (var i = 0; i < _this.multipleSelection.length; i++) {
                        var data = _this.multipleSelection[i];
                        var tem = {
                            pkId: data.pkId,
                            msgId: msgId,
                            //fldNm: data.fldNm,
                            fldNm: data.path,
                            fldDesc: data.fldDesc,
                            fldLen: data.fldLen,
                            isReq: data.isReq,
                            defVal: data.defVal,
                            fixVal: data.fixVal
                        };
                        comitData.ymitMsgFld.push(tem);

                    }

                    yufp.service.request({
                        method: 'POST',
                        url: _this.saveTelUrl,
                        data: comitData,
                        callback: function (code, message, response) {
                            if (response.code == 0) {
                                _this.$message({message: '数据保存成功！'});
                            } else {
                                _this.$message({message: '数据保存失败！'});
                            }
                        }
                    });
                },

                //数据集列表中数据删除
                reqMsgPointDeleteFn: function () {
                    if (this.$refs.reqMsgTable.selections.length < 1) {
                        this.$message({message: '请先选择一条记录', type: 'warning'})
                        return;
                    }
                    var _this = this;
                    if (_this.$refs.reqMsgTable.selections) {
                        var ids = '';
                        for (var i = 0; i < _this.$refs.reqMsgTable.selections.length; i++) {
                            ids = ids + ',' + _this.$refs.reqMsgTable.selections[i].pkId;
                        }
                        _this.$confirm('删除控制点将删除其数据权限及授权数据,确定删除?', '提示', {
                            confirmButtonText: '确定',
                            cancelButtonText: '取消',
                            type: 'warning'
                        }).then(function () {
                            yufp.service.request({
                                method: 'POST',
                                url: _this.reqMsgDelUrl + ids,
                                callback: function (code, message, response) {
                                    if (response.code == 0) {
                                        _this.$message({message: '数据删除成功！'});
                                        var param = {
                                            condition: JSON.stringify({
                                                parConstId: _this.funcId ? _this.funcId : null
                                            })
                                        };
                                        _this.$refs.reqMsgTable.remoteData(param);
                                    }
                                    else {
                                        _this.$message({message: '数据删除失败！'});
                                    }
                                }
                            });
                        })
                    }
                },
                rowClick: function (row) {
                    if (this.$refs.reqMsgTable.selections == 0) {
                        this.$refs.reqMsgTable.selections.push(row.row);
                    }
                    this.reqMsgDialogVisible = true;
                    this.$nextTick(function () {
                        this.viewType = 'EDIT';
                        this.addOrUpd = 'EDIT';
                        this.reqMsgFormButtons[2].hidden = false;
                        this.reqMsgFormButtons[1].hidden = true;
                        yufp.extend(this.$refs.reqMsgForm.formModel, this.$refs.reqMsgTable.selections[0]);
                        /*加载数据集的字段信息*/
                        this.initSetIns();
                    });
                    this.reqMsgModifyFn();
                    this.$refs.reqMsgTable.clearSelection();
                },
                uploadUrl: function () {
                    var url = yufp.service.getUrl({
                        url: backend.adminService + "/api/excelio/import"
                    });
                    return yufp.util.addTokenInfo(url);
                },
                handleRemove: function (file, fileList) {
                    this.$message('handleRemove');
                },
                handleUploadSuccess: function (response, file, fileList) {
                    taskId = response.taskId;
                    if (taskId != "failure") {
                        vm.queryInfoFn();
                    }
                },
                exportFn: function () {
                    yufp.service.request({
                        needToken: true,
                        url: backend.adminService + '/api/excelio/export',//?fetchSize=12
                        method: 'GET',
                        callback: function (code, message, response) {
                            taskId = response.taskId;
                            vm.queryInfoFn();
                        }
                    });
                },
                exportTemplateFn: function () {
                    var url = yufp.settings.ssl ? 'https://' : 'http://';
                    url += yufp.settings.url;
                    url += backend.adminService;
                    url += "/api/excelio/exportTemplate";//?fileName=abc.xlsx
                    window.open(yufp.util.addTokenInfo(url));
                },
                importFn: function () {
                    gg = this.$refs.upload;
                    this.$refs.upload.submit();
                },
                /*-------------------↑↑↑↑↑↑↑↑↑----------------------测试用例相关的函数---------↑↑↑↑↑↑↑↑↑----------------------*/
                /*-------------------↓↓↓↓↓↓----------------------请求报文treegrid用例相关的函数------------↓↓↓↓↓-------------------*/
                changeFun(val) {

                    this.multipleSelection = val;
                },
                rowShowControl: function (row) {
                    let show = (row._parent ? (row._parent._expanded && row._parent._show) : true);
                    row._show = show;
                    return show ? '' : 'display:none;';
                },
                rowHasChildren: function (row) {
                    if (row.children && row.children.length > 0) {
                        return true
                    }
                    return false
                },
                toggleChildrenShow: function (row) {
                    row._expanded = !row._expanded;
                },
                treeToArray: function (data, parent, level, expandedAll) {
                    let tmp = [];
                    const _this = this;
                    if (data != null) {
                        Array.from(data).forEach(function (record) {
                            if (record._expanded === undefined) {
                                Vue.set(record, '_expanded', expandedAll)
                            }
                            if (parent) {
                                Vue.set(record, '_parent', parent)
                            }
                            let _level = 0;
                            if (level !== undefined && level !== null) {
                                _level = level + 1
                            }
                            Vue.set(record, '_level', _level);
                            tmp.push(record);
                            if (record.children && record.children.length > 0) {
                                let children = _this.treeToArray(record.children, record, _level, expandedAll);
                                tmp = tmp.concat(children)
                            }
                        });
                    }
                    return tmp
                },
                /*初始化的时候需要msgid*/
                initMsgID: function () {
                    var _this = this;
                    yufp.service.request({
                        method: 'GET',
                        url: "/api/ymittrade/" + pathId,
                        callback: function (code, message, response) {
                            var initData = response.data;
                            if (initData != null) {
                                msgId = initData.respPath;
                                _this.dataLoad();
                            }
                        }
                    })
                },
                dataLoad: function () {
                    var id = msgId;
                    var param = {
                        condition: JSON.stringify({
                            pkId: id,
                        })
                    };
                    var me = this;
                    yufp.service.request({
                        url: this.reqMsgFldUrl,
                        data: param,
                        method: 'GET',
                        callback: function (code, message, response) {
                            if (response.code == 0) {
                                me.gridData = response.data;
                            }
                        }
                    });
                },

                temLoad: function () {

                    var param = {
                        condition: JSON.stringify({
                            trdInfId: pathId,
                            msgId: msgId,
                        })
                    };
                    var me = this;
                    yufp.service.request({
                        url: backend.adminService + '/api/biztmp/gettmp',
                        data: param,
                        async: false,
                        method: 'GET',
                        callback: function (code, message, response) {
                            if (response.code == 0) {
                                var tmp = response.data;
                                yufp.extend(me.$refs.busTel.formModel, tmp);
                                me.gridDataTmp = response.data.ymitMsgFld;
                            }
                        }
                    });
                },
                temFldLoad: function () {

                    var param = {
                        condition: JSON.stringify({
                            trdInfId: pathId,
                            msgId: msgId,
                        })
                    };
                    var me = this;
                    yufp.service.request({
                        url: backend.adminService + '/api/biztmp/gettmp',
                        data: param,
                        async: false,
                        method: 'GET',
                        callback: function (code, message, response) {
                            if (response.code == 0) {
                                var tmp = response.data;
                                yufp.extend(me.$refs.busTel.formModel, tmp);
                                me.reqMsgSetForm.data = response.data.ymitMsgFld;
                            }
                        }
                    });
                },
                allDataLoad: function () {
                    var id = msgId;
                    var param = {
                        condition: JSON.stringify({
                            pkId: id,
                        })
                    };
                    var me = this;
                    yufp.service.request({
                        url: backend.adminService + '/api/bspmsg/getfullfld',
                        data: param,
                        method: 'GET',
                        callback: function (code, message, response) {
                            if (response.code == 0) {
                                me.reqMsgSetForm.data = response.data;
                            }
                        }
                    });
                },

                tabClick: function (tab, event) {
                    if (tab.label == '全量字段') {
                        this.dataLoad();
                    }
                    if (tab.label == '业务模板') {
                        this.temLoad();
                    }
                    if (tab.label == '数据集') {
                        var param = {
                            condition: JSON.stringify({
                                trdInfId: pathId,
                            })
                        };
                        this.$refs.reqMsgTable.remoteData(param);
                    }
                },
                /*-------------------↑↑↑↑↑↑↑↑↑----------------------treegrid数---------↑↑↑↑↑↑↑↑↑----------------------*/

            },
            mounted: function () {
                this.initMsgID();
                this.loadChgLog();
            },
            computed: {
                data: function () {
                    return this.treeToArray(this.gridData, null, null, false);
                },
                dataTmp: function () {
                    //this.temLoad();
                    return this.treeToArray(this.gridDataTmp, null, null, false);
                }
            }

        });
        bus.$on('aaaa', function (value) {
            vm.reqId = value;
            console.log(value);
        });
    };
});